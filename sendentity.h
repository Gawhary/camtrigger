#ifndef SENDENTITY_H
#define SENDENTITY_H

#include <QFrame>
#include <QTime>

namespace Ui {
    class SendEntity;
}

class SendEntity : public QFrame
{
    Q_OBJECT

    Q_PROPERTY(QTime time READ time WRITE setTime)
    Q_PROPERTY(int imgCount READ imgCount WRITE setImgCount)

public:
    explicit SendEntity(QTime time, int lastImgs = 10, QWidget *parent = 0);
    ~SendEntity();
    QTime time();
    int imgCount();

public slots:
    void setTime(QTime arg)
    {
        m_time = arg;
    }

    void setImgCount(int arg)
    {
        m_imgCount = arg;
    }

private:
    Ui::SendEntity *ui;
    QTime m_time;
    int m_imgCount;
};

#endif // SENDENTITY_H
