#ifndef TIMEENTITY_H
#define TIMEENTITY_H

#include <QWidget>
#include <QTime>

namespace Ui {
    class TimeEntity;
}

class TimeEntity : public QWidget
{
    Q_OBJECT

public:
    explicit TimeEntity(QTime time,QWidget *parent = 0);
    ~TimeEntity();
    QTime time();

private:
    Ui::TimeEntity *ui;
    QTime m_time;
};

#endif // TIMEENTITY_H
