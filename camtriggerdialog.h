#ifndef SETTINGSDIALOG_H
#define SETTINGSDIALOG_H

#include <QDialog>
#include <QTime>
#include <QGridLayout>
#include <QTimer>
#include <QCameraViewfinder>

#include "CamTrigger.h"

namespace Ui {
    class SettingsDialog;
}

class CamTriggerDialog : public QDialog
{
    Q_OBJECT

public:

     enum ScreenOrientation {
        ScreenOrientationLockPortrait,
        ScreenOrientationLockLandscape,
        ScreenOrientationAuto
    };

    explicit CamTriggerDialog(QWidget *parent = 0);
    ~CamTriggerDialog();

    void setOrientation(ScreenOrientation orientation);
    void showExpanded();




public slots:

    void onTurnOnCameraPreview();
    void onTurnOffCameraPreview();

private slots:
    void on_displayButton_clicked();
    void on_cameraButton_clicked();
    void on_TimersButton_clicked();
    void on_storageButton_clicked();
    void on_modefyButton_clicked();
    void on_sendButton_clicked();
    void goToSettingsPage(int index = 0);
    void backToMainPage();
    void saveUI();
    void loadUI();
    void on_addTimeButton_clicked();
    void onExitAction();
    void on_saveToButton_clicked();
    void on_addSendTimeButton_clicked();
    void onStartTriggerAction();
    void onStopTriggerAction();
    void addCaptureTimer(QTime time);
    void addSendTimer(QTime time, int imgCount);
    void test();
    void on_cropStartXSpinBox_editingFinished();
    void on_cropStartYSpinBox_editingFinished();
    void on_cropEndXSpinBox_editingFinished();
    void on_cropEndYSpinBox_editingFinished();
    void on_cameraResolutionComboBox_currentIndexChanged(int index);   
    void turnOnBacklight();

private:
    Ui::SettingsDialog *ui;
    QAction *m_ExitAction;
    QAction *m_okAction;
    QAction *m_cancelAction;
    QAction *m_startTrigger;
    QAction *m_stopTrigger;

    QWidget *m_timersScrollWidget;
    QGridLayout *m_timersLayout;
    QWidget *m_sendTimersScrollWidget;
    QGridLayout *m_sendTimersLayout;

    CamTrigger *m_camTrigger;
    QCameraViewfinder* m_viewFinder;
    QTimer *turnOnBacklightTimer;

//    QList<QSize> resolutions;

};

#endif // SETTINGSDIALOG_H
