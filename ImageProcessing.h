#ifndef IMAGEPROCESSING_H
#define IMAGEPROCESSING_H
#include <QImage>
#include "math.h"

//template<class T>
///*inline*/ const int kClamp( const int x, const int low, const int high )
//{
//    if ( x < low )       return low;
//    else if ( high < x ) return high;
//    else                 return x;
//}

//inline
//int changeBrightness( int value, int brightness )
//    {
//    return kClamp( value + brightness * 255 / 100, 0, 255 );
//    }

//inline
//int changeContrast( int value, int contrast )
//    {
//    return kClamp((( value - 127 ) * ((contrast + 100) / 200) ) + 127, 0, 255 );
//    }

//inline
int changeUsingTable( int value, const int table[] )
    {
    return table[ value ];
    }

//template< int operation( int, int ) >
static
QImage changeImageBrightness( const QImage& image, int value )
    {
    QImage im = image;
    im.detach();
//    if( im.numColors() == 0 ) /* truecolor */
//        {
//        if( im.format() != QImage::Format_RGB32 ) /* just in case */
//            im = im.convertToFormat( QImage::Format_RGB32 );
//        int table[ 256 ];
//        for( int i = 0;
//             i < 256;
//             ++i )
//            table[ i ] = operation( i, value );
//        if( im.hasAlphaChannel() )
//            {
//            for( int y = 0;
//                 y < im.height();
//                 ++y )
//                {
//                QRgb* line = reinterpret_cast< QRgb* >( im.scanLine( y ));
//                for( int x = 0;
//                     x < im.width();
//                     ++x )
//                    line[ x ] = qRgba( changeUsingTable( qRed( line[ x ] ), table ),
//                        changeUsingTable( qGreen( line[ x ] ), table ),
//                        changeUsingTable( qBlue( line[ x ] ), table ),
//                        changeUsingTable( qAlpha( line[ x ] ), table ));
//                }
//            }
//        else
//            {
//            for( int y = 0;
//                 y < im.height();
//                 ++y )
//                {
//                QRgb* line = reinterpret_cast< QRgb* >( im.scanLine( y ));
//                for( int x = 0;
//                     x < im.width();
//                     ++x )
//                    line[ x ] = qRgb( changeUsingTable( qRed( line[ x ] ), table ),
//                        changeUsingTable( qGreen( line[ x ] ), table ),
//                        changeUsingTable( qBlue( line[ x ] ), table ));
//                }
//            }
//        }
//    else
        {
        QVector<QRgb> colors = im.colorTable();
        for( int i = 0;
             i < im.numColors();
             ++i ){
//            colors[ i ] = qRgb( ,
//                ,
//                );
//            int r = operation( qRed( colors[ i ] ), value );
//            int g = operation( qGreen( colors[ i ] ), value );
//            int b = operation( qBlue( colors[ i ] ), value );

            int r = qRed( colors[ i ] );
            int g = qGreen( colors[ i ] );
            int b = qBlue( colors[ i ] );

            r = (r + value * 255 / 100);
            if(r < 0) r = 0;
            else if(r > 255) r = 255;
//            g = qBound( 0, g + value * 255 / 100, 255 );
//            b = qBound( 0, b + value * 255 / 100, 255 );
            colors[ i ] = qRgb(r,g,b);
        }
        }
    return im;
    }


// brightness is multiplied by 100 in order to avoid floating point numbers
QImage changeBrightness( const QImage& image, int brightness )
    {
    if( brightness == 0 ) // no change
        return image;
    return changeImageBrightness ( image, brightness );
    }


// contrast is multiplied by 100 in order to avoid floating point numbers
QImage changeContrast( const QImage& image, int contrast )
    {
//    if( contrast == 0 ) // no change
        return image;
//    return changeImage< changeContrast >( image, contrast );
    }

#endif // IMAGEPROCESSING_H
