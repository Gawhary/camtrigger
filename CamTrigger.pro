# Add files and directories to ship with the application
# by adapting the examples below.
# file1.source = myfile
# dir1.source = mydir
# file1 dir1
DEPLOYMENTFOLDERS = 

# Avoid auto screen rotation
# DEFINES += ORIENTATIONLOCK
# Needs to be defined for Symbian
DEFINES += NETWORKACCESS


# Smart Installer package's UID
# This UID is from the protected range
# and therefore the package will fail to install if self-signed
# By default qmake uses the unprotected range value if unprotected UID is defined for the application
# and 0x2002CCCF value if protected UID is given to the application
# symbian:DEPLOYMENT.installer_header = 0x2002CCCF
QT += xml

# If your application uses the Qt Mobility libraries, uncomment
# the following lines and add the respective components to the
# MOBILITY variable.
CONFIG += mobility
MOBILITY += multimedia
MOBILITY += messaging
MOBILITY += systeminfo
symbian:TARGET.CAPABILITY = NetworkServices \
    LocalServices \
    ReadUserData \
    WriteUserData \
    UserEnvironment
SOURCES += CamTrigger.rss \
    CamTrigger_reg.rss \
    main.cpp \
    timeentity.cpp \
    sendentity.cpp \
    camtriggerdialog.cpp \
    CamTrigger.cpp
HEADERS += CamTrigger.loc \
    timeentity.h \
    sendentity.h \
    camtriggerdialog.h \
    CamTrigger.h
FORMS += settingsdialog.ui \
    timeentity.ui \
    sendentity.ui

# INCLUDEPATH += "D:/ProgramFiles/Nokia/QtSDK/Symbian/SDK/epoc32/release/armv5/lib"
symbian:LIBS += -lhal \
    -leuser

# switch the following two block for Signed/Unsigned version (turn off network)

DEFINES += SELFSIGNED
symbian:TARGET.UID3 = 0xEB7FD4A9

# symbian:TARGET.CAPABILITY += WriteDeviceData
# symbian:LIBS += -lprofileengine -letel3rdparty -lfeatdiscovery
# symbian:SOURCES += xqprofile_p.cpp \
# xqprofile.cpp \
# cflightmodechecker.cpp
# symbian:HEADERS += \
# xqprofile_p.h \
# xqprofile.h \
# cflightmodechecker.h

# Please do not modify the following two lines. Required for deployment.
include(deployment.pri)
qtcAddDeployment()
RESOURCES += RC.qrc
