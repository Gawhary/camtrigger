#include "sendentity.h"
#include "ui_sendentity.h"

SendEntity::SendEntity(QTime time, int lastImgs, QWidget *parent) :
    QFrame(parent),
    ui(new Ui::SendEntity)
{
    m_time = time;
    m_imgCount = lastImgs;
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);
    ui->lcdNumber->display(time.toString("hh:mm AP"));
    ui->spinBox->setValue(lastImgs);
}

SendEntity::~SendEntity()
{
    delete ui;
}

QTime SendEntity::time()
{
    return m_time;
}

int SendEntity::imgCount()
{
    return m_imgCount;
}
