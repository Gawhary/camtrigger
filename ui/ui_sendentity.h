/********************************************************************************
** Form generated from reading UI file 'sendentity.ui'
**
** Created: Fri Mar 4 01:45:55 2011
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SENDENTITY_H
#define UI_SENDENTITY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLCDNumber>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QToolButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SendEntity
{
public:
    QHBoxLayout *horizontalLayout;
    QLCDNumber *lcdNumber;
    QSpacerItem *horizontalSpacer;
    QSpinBox *spinBox;
    QSpacerItem *horizontalSpacer_2;
    QToolButton *toolButton;

    void setupUi(QWidget *SendEntity)
    {
        if (SendEntity->objectName().isEmpty())
            SendEntity->setObjectName(QString::fromUtf8("SendEntity"));
        SendEntity->resize(132, 23);
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(SendEntity->sizePolicy().hasHeightForWidth());
        SendEntity->setSizePolicy(sizePolicy);
        horizontalLayout = new QHBoxLayout(SendEntity);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lcdNumber = new QLCDNumber(SendEntity);
        lcdNumber->setObjectName(QString::fromUtf8("lcdNumber"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lcdNumber->sizePolicy().hasHeightForWidth());
        lcdNumber->setSizePolicy(sizePolicy1);
        lcdNumber->setNumDigits(9);

        horizontalLayout->addWidget(lcdNumber);

        horizontalSpacer = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);

        spinBox = new QSpinBox(SendEntity);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        QSizePolicy sizePolicy2(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(spinBox->sizePolicy().hasHeightForWidth());
        spinBox->setSizePolicy(sizePolicy2);
        spinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox->setMaximum(999);

        horizontalLayout->addWidget(spinBox);

        horizontalSpacer_2 = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        toolButton = new QToolButton(SendEntity);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        sizePolicy1.setHeightForWidth(toolButton->sizePolicy().hasHeightForWidth());
        toolButton->setSizePolicy(sizePolicy1);

        horizontalLayout->addWidget(toolButton);


        retranslateUi(SendEntity);
        QObject::connect(toolButton, SIGNAL(clicked()), SendEntity, SLOT(close()));

        QMetaObject::connectSlotsByName(SendEntity);
    } // setupUi

    void retranslateUi(QWidget *SendEntity)
    {
        SendEntity->setWindowTitle(QApplication::translate("SendEntity", "Form", 0, QApplication::UnicodeUTF8));
        spinBox->setSpecialValueText(QApplication::translate("SendEntity", "10", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("SendEntity", "X", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SendEntity: public Ui_SendEntity {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SENDENTITY_H
