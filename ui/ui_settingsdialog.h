/********************************************************************************
** Form generated from reading UI file 'settingsdialog.ui'
**
** Created: Fri Mar 4 01:45:55 2011
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGSDIALOG_H
#define UI_SETTINGSDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QCheckBox>
#include <QtGui/QComboBox>
#include <QtGui/QDialog>
#include <QtGui/QFrame>
#include <QtGui/QGridLayout>
#include <QtGui/QGroupBox>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QScrollArea>
#include <QtGui/QSlider>
#include <QtGui/QSpacerItem>
#include <QtGui/QSpinBox>
#include <QtGui/QStackedWidget>
#include <QtGui/QTabWidget>
#include <QtGui/QTimeEdit>
#include <QtGui/QToolButton>
#include <QtGui/QVBoxLayout>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SettingsDialog
{
public:
    QVBoxLayout *verticalLayout_3;
    QStackedWidget *stackedWidget;
    QWidget *page;
    QVBoxLayout *verticalLayout_4;
    QFrame *frame;
    QHBoxLayout *horizontalLayout_54;
    QSpacerItem *horizontalSpacer_15;
    QVBoxLayout *verticalLayout_15;
    QSpacerItem *verticalSpacer_35;
    QPushButton *displayButton;
    QSpacerItem *verticalSpacer_36;
    QPushButton *cameraButton;
    QSpacerItem *verticalSpacer_37;
    QPushButton *TimersButton;
    QSpacerItem *verticalSpacer_38;
    QPushButton *storageButton;
    QSpacerItem *verticalSpacer_39;
    QPushButton *modefyButton;
    QSpacerItem *verticalSpacer_40;
    QPushButton *sendButton;
    QSpacerItem *verticalSpacer_41;
    QSpacerItem *horizontalSpacer_14;
    QWidget *page_2;
    QGridLayout *gridLayout;
    QTabWidget *tabWidget;
    QWidget *displayTab;
    QVBoxLayout *verticalLayout_6;
    QSpacerItem *verticalSpacer_16;
    QGroupBox *groupBox_4;
    QVBoxLayout *verticalLayout_2;
    QHBoxLayout *horizontalLayout_4;
    QCheckBox *turnOnDisplayCheckBox;
    QSpacerItem *horizontalSpacer;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_4;
    QSpinBox *displaySecBeforeSpinBox;
    QLabel *label_2;
    QSpacerItem *horizontalSpacer_3;
    QHBoxLayout *horizontalLayout_2;
    QSpacerItem *horizontalSpacer_6;
    QSpinBox *displaySecDurationSpinBox;
    QLabel *label_3;
    QSpacerItem *horizontalSpacer_2;
    QSpacerItem *verticalSpacer_5;
    QGroupBox *powerSaveGroupBox;
    QVBoxLayout *verticalLayout_8;
    QHBoxLayout *horizontalLayout_3;
    QCheckBox *turnOfGsmCheckBox;
    QSpacerItem *horizontalSpacer_17;
    QSpacerItem *verticalSpacer_15;
    QWidget *cameraTab;
    QVBoxLayout *verticalLayout;
    QSpacerItem *verticalSpacer_19;
    QCheckBox *flachOnCheckBox;
    QSpacerItem *verticalSpacer_20;
    QHBoxLayout *horizontalLayout_6;
    QLabel *label_6;
    QSpinBox *spinBox;
    QSlider *ContrastSlider;
    QHBoxLayout *horizontalLayout_7;
    QLabel *label_7;
    QSpinBox *spinBox_2;
    QSlider *BrightnessSlider;
    QSpacerItem *verticalSpacer_21;
    QHBoxLayout *horizontalLayout_14;
    QLabel *label_4;
    QComboBox *focusModeComboBox;
    QSpacerItem *verticalSpacer_22;
    QHBoxLayout *horizontalLayout_9;
    QLabel *label_9;
    QSpinBox *spinBox_3;
    QSlider *zoomSlider;
    QSpacerItem *verticalSpacer_23;
    QHBoxLayout *horizontalLayout_10;
    QLabel *label_8;
    QComboBox *cameraResolutionComboBox;
    QSpacerItem *verticalSpacer_24;
    QWidget *timersTab;
    QGridLayout *gridLayout_5;
    QHBoxLayout *horizontalLayout_11;
    QScrollArea *timersScrollArea;
    QWidget *scrollAreaWidgetContents;
    QGridLayout *gridLayout_3;
    QTimeEdit *addTimeEdit;
    QPushButton *addTimeButton;
    QWidget *storageTab;
    QVBoxLayout *verticalLayout_5;
    QSpacerItem *verticalSpacer_4;
    QGroupBox *groupBox_2;
    QGridLayout *gridLayout_10;
    QHBoxLayout *horizontalLayout_12;
    QLineEdit *saveToEdit;
    QToolButton *saveToButton;
    QSpacerItem *verticalSpacer_3;
    QGroupBox *groupBox_3;
    QGridLayout *gridLayout_11;
    QHBoxLayout *horizontalLayout_13;
    QLineEdit *fileNamePrefixLineEdit;
    QLabel *label_14;
    QSpacerItem *verticalSpacer_2;
    QWidget *modefyTab;
    QVBoxLayout *verticalLayout_9;
    QSpacerItem *verticalSpacer;
    QCheckBox *modefyCheckBox;
    QSpacerItem *verticalSpacer_6;
    QHBoxLayout *horizontalLayout_17;
    QLabel *label_19;
    QSpinBox *spinBox_4;
    QSlider *modefyBrightnesSlider;
    QHBoxLayout *horizontalLayout_16;
    QLabel *label_18;
    QSpinBox *spinBox_5;
    QSlider *modefyContrastSlider;
    QSpacerItem *verticalSpacer_7;
    QHBoxLayout *horizontalLayout_8;
    QLabel *label_13;
    QSpinBox *resizeToWidthSpinBox;
    QLabel *label_15;
    QSpinBox *resizeToHeightSpinBox;
    QSpacerItem *horizontalSpacer_7;
    QHBoxLayout *horizontalLayout_15;
    QLabel *label_5;
    QComboBox *modefyQualityComboBox;
    QSpacerItem *verticalSpacer_8;
    QCheckBox *cropCheckBox;
    QHBoxLayout *horizontalLayout_18;
    QSpacerItem *horizontalSpacer_8;
    QLabel *label_20;
    QSpinBox *cropStartXSpinBox;
    QLabel *label_23;
    QSpinBox *cropStartYSpinBox;
    QSpacerItem *horizontalSpacer_11;
    QHBoxLayout *horizontalLayout_19;
    QSpacerItem *horizontalSpacer_9;
    QLabel *label_22;
    QSpinBox *cropEndXSpinBox;
    QLabel *label_70;
    QSpinBox *cropEndYSpinBox;
    QSpacerItem *horizontalSpacer_12;
    QSpacerItem *verticalSpacer_9;
    QWidget *sendTab;
    QGridLayout *gridLayout_7;
    QCheckBox *sendMmsCheckBox;
    QLabel *label_74;
    QLineEdit *sendTolineEdit;
    QGridLayout *gridLayout_4;
    QLabel *label_72;
    QSpacerItem *horizontalSpacer_13;
    QLabel *label_73;
    QScrollArea *sendScrollArea;
    QWidget *scrollAreaWidgetContents_4;
    QSpacerItem *horizontalSpacer_5;
    QLabel *label;
    QHBoxLayout *horizontalLayout_52;
    QTimeEdit *addSendTimeEdit;
    QSpinBox *sendImgCountSpinBox;
    QPushButton *addSendTimeButton;
    QWidget *page_3;
    QVBoxLayout *verticalLayout_7;
    QFrame *cameraFrame;

    void setupUi(QDialog *SettingsDialog)
    {
        if (SettingsDialog->objectName().isEmpty())
            SettingsDialog->setObjectName(QString::fromUtf8("SettingsDialog"));
        SettingsDialog->resize(222, 302);
        QIcon icon;
        icon.addFile(QString::fromUtf8(":/cam.png"), QSize(), QIcon::Normal, QIcon::Off);
        SettingsDialog->setWindowIcon(icon);
        verticalLayout_3 = new QVBoxLayout(SettingsDialog);
        verticalLayout_3->setSpacing(0);
        verticalLayout_3->setContentsMargins(0, 0, 0, 0);
        verticalLayout_3->setObjectName(QString::fromUtf8("verticalLayout_3"));
        verticalLayout_3->setSizeConstraint(QLayout::SetNoConstraint);
        stackedWidget = new QStackedWidget(SettingsDialog);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        QSizePolicy sizePolicy(QSizePolicy::Expanding, QSizePolicy::Expanding);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(stackedWidget->sizePolicy().hasHeightForWidth());
        stackedWidget->setSizePolicy(sizePolicy);
        stackedWidget->setFrameShape(QFrame::NoFrame);
        stackedWidget->setLineWidth(0);
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        verticalLayout_4 = new QVBoxLayout(page);
        verticalLayout_4->setObjectName(QString::fromUtf8("verticalLayout_4"));
        frame = new QFrame(page);
        frame->setObjectName(QString::fromUtf8("frame"));
        QSizePolicy sizePolicy1(QSizePolicy::Preferred, QSizePolicy::Preferred);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(frame->sizePolicy().hasHeightForWidth());
        frame->setSizePolicy(sizePolicy1);
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        horizontalLayout_54 = new QHBoxLayout(frame);
        horizontalLayout_54->setObjectName(QString::fromUtf8("horizontalLayout_54"));
        horizontalSpacer_15 = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_54->addItem(horizontalSpacer_15);

        verticalLayout_15 = new QVBoxLayout();
        verticalLayout_15->setObjectName(QString::fromUtf8("verticalLayout_15"));
        verticalSpacer_35 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_15->addItem(verticalSpacer_35);

        displayButton = new QPushButton(frame);
        displayButton->setObjectName(QString::fromUtf8("displayButton"));
        QSizePolicy sizePolicy2(QSizePolicy::Preferred, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(displayButton->sizePolicy().hasHeightForWidth());
        displayButton->setSizePolicy(sizePolicy2);

        verticalLayout_15->addWidget(displayButton);

        verticalSpacer_36 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_15->addItem(verticalSpacer_36);

        cameraButton = new QPushButton(frame);
        cameraButton->setObjectName(QString::fromUtf8("cameraButton"));
        sizePolicy2.setHeightForWidth(cameraButton->sizePolicy().hasHeightForWidth());
        cameraButton->setSizePolicy(sizePolicy2);

        verticalLayout_15->addWidget(cameraButton);

        verticalSpacer_37 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_15->addItem(verticalSpacer_37);

        TimersButton = new QPushButton(frame);
        TimersButton->setObjectName(QString::fromUtf8("TimersButton"));
        sizePolicy2.setHeightForWidth(TimersButton->sizePolicy().hasHeightForWidth());
        TimersButton->setSizePolicy(sizePolicy2);

        verticalLayout_15->addWidget(TimersButton);

        verticalSpacer_38 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_15->addItem(verticalSpacer_38);

        storageButton = new QPushButton(frame);
        storageButton->setObjectName(QString::fromUtf8("storageButton"));
        sizePolicy2.setHeightForWidth(storageButton->sizePolicy().hasHeightForWidth());
        storageButton->setSizePolicy(sizePolicy2);

        verticalLayout_15->addWidget(storageButton);

        verticalSpacer_39 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_15->addItem(verticalSpacer_39);

        modefyButton = new QPushButton(frame);
        modefyButton->setObjectName(QString::fromUtf8("modefyButton"));
        sizePolicy2.setHeightForWidth(modefyButton->sizePolicy().hasHeightForWidth());
        modefyButton->setSizePolicy(sizePolicy2);

        verticalLayout_15->addWidget(modefyButton);

        verticalSpacer_40 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_15->addItem(verticalSpacer_40);

        sendButton = new QPushButton(frame);
        sendButton->setObjectName(QString::fromUtf8("sendButton"));
        sizePolicy2.setHeightForWidth(sendButton->sizePolicy().hasHeightForWidth());
        sendButton->setSizePolicy(sizePolicy2);

        verticalLayout_15->addWidget(sendButton);

        verticalSpacer_41 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_15->addItem(verticalSpacer_41);


        horizontalLayout_54->addLayout(verticalLayout_15);

        horizontalSpacer_14 = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_54->addItem(horizontalSpacer_14);


        verticalLayout_4->addWidget(frame);

        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        gridLayout = new QGridLayout(page_2);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        tabWidget = new QTabWidget(page_2);
        tabWidget->setObjectName(QString::fromUtf8("tabWidget"));
        tabWidget->setLayoutDirection(Qt::LeftToRight);
        tabWidget->setMovable(false);
        displayTab = new QWidget();
        displayTab->setObjectName(QString::fromUtf8("displayTab"));
        verticalLayout_6 = new QVBoxLayout(displayTab);
        verticalLayout_6->setObjectName(QString::fromUtf8("verticalLayout_6"));
        verticalSpacer_16 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_16);

        groupBox_4 = new QGroupBox(displayTab);
        groupBox_4->setObjectName(QString::fromUtf8("groupBox_4"));
        verticalLayout_2 = new QVBoxLayout(groupBox_4);
        verticalLayout_2->setSpacing(9);
        verticalLayout_2->setObjectName(QString::fromUtf8("verticalLayout_2"));
        horizontalLayout_4 = new QHBoxLayout();
        horizontalLayout_4->setObjectName(QString::fromUtf8("horizontalLayout_4"));
        turnOnDisplayCheckBox = new QCheckBox(groupBox_4);
        turnOnDisplayCheckBox->setObjectName(QString::fromUtf8("turnOnDisplayCheckBox"));
        QSizePolicy sizePolicy3(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy3.setHorizontalStretch(0);
        sizePolicy3.setVerticalStretch(0);
        sizePolicy3.setHeightForWidth(turnOnDisplayCheckBox->sizePolicy().hasHeightForWidth());
        turnOnDisplayCheckBox->setSizePolicy(sizePolicy3);
        turnOnDisplayCheckBox->setMinimumSize(QSize(0, 0));
        turnOnDisplayCheckBox->setTristate(false);

        horizontalLayout_4->addWidget(turnOnDisplayCheckBox);

        horizontalSpacer = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_4->addItem(horizontalSpacer);


        verticalLayout_2->addLayout(horizontalLayout_4);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        horizontalSpacer_4 = new QSpacerItem(15, 0, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_4);

        displaySecBeforeSpinBox = new QSpinBox(groupBox_4);
        displaySecBeforeSpinBox->setObjectName(QString::fromUtf8("displaySecBeforeSpinBox"));
        displaySecBeforeSpinBox->setEnabled(false);
        displaySecBeforeSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        displaySecBeforeSpinBox->setMinimum(1);
        displaySecBeforeSpinBox->setMaximum(60);
        displaySecBeforeSpinBox->setValue(30);

        horizontalLayout->addWidget(displaySecBeforeSpinBox);

        label_2 = new QLabel(groupBox_4);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        QSizePolicy sizePolicy4(QSizePolicy::Minimum, QSizePolicy::Preferred);
        sizePolicy4.setHorizontalStretch(0);
        sizePolicy4.setVerticalStretch(0);
        sizePolicy4.setHeightForWidth(label_2->sizePolicy().hasHeightForWidth());
        label_2->setSizePolicy(sizePolicy4);
        label_2->setWordWrap(true);

        horizontalLayout->addWidget(label_2);

        horizontalSpacer_3 = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_3);


        verticalLayout_2->addLayout(horizontalLayout);

        horizontalLayout_2 = new QHBoxLayout();
        horizontalLayout_2->setObjectName(QString::fromUtf8("horizontalLayout_2"));
        horizontalSpacer_6 = new QSpacerItem(15, 0, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_6);

        displaySecDurationSpinBox = new QSpinBox(groupBox_4);
        displaySecDurationSpinBox->setObjectName(QString::fromUtf8("displaySecDurationSpinBox"));
        displaySecDurationSpinBox->setEnabled(false);
        displaySecDurationSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        displaySecDurationSpinBox->setMinimum(1);
        displaySecDurationSpinBox->setMaximum(60);
        displaySecDurationSpinBox->setValue(35);

        horizontalLayout_2->addWidget(displaySecDurationSpinBox);

        label_3 = new QLabel(groupBox_4);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        sizePolicy4.setHeightForWidth(label_3->sizePolicy().hasHeightForWidth());
        label_3->setSizePolicy(sizePolicy4);
        label_3->setWordWrap(true);

        horizontalLayout_2->addWidget(label_3);

        horizontalSpacer_2 = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_2->addItem(horizontalSpacer_2);


        verticalLayout_2->addLayout(horizontalLayout_2);


        verticalLayout_6->addWidget(groupBox_4);

        verticalSpacer_5 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_5);

        powerSaveGroupBox = new QGroupBox(displayTab);
        powerSaveGroupBox->setObjectName(QString::fromUtf8("powerSaveGroupBox"));
        powerSaveGroupBox->setEnabled(true);
        verticalLayout_8 = new QVBoxLayout(powerSaveGroupBox);
        verticalLayout_8->setObjectName(QString::fromUtf8("verticalLayout_8"));
        horizontalLayout_3 = new QHBoxLayout();
        horizontalLayout_3->setObjectName(QString::fromUtf8("horizontalLayout_3"));
        turnOfGsmCheckBox = new QCheckBox(powerSaveGroupBox);
        turnOfGsmCheckBox->setObjectName(QString::fromUtf8("turnOfGsmCheckBox"));
        turnOfGsmCheckBox->setEnabled(true);
        sizePolicy3.setHeightForWidth(turnOfGsmCheckBox->sizePolicy().hasHeightForWidth());
        turnOfGsmCheckBox->setSizePolicy(sizePolicy3);
        turnOfGsmCheckBox->setMinimumSize(QSize(0, 0));
        turnOfGsmCheckBox->setTristate(false);

        horizontalLayout_3->addWidget(turnOfGsmCheckBox);

        horizontalSpacer_17 = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_3->addItem(horizontalSpacer_17);


        verticalLayout_8->addLayout(horizontalLayout_3);


        verticalLayout_6->addWidget(powerSaveGroupBox);

        verticalSpacer_15 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_6->addItem(verticalSpacer_15);

        tabWidget->addTab(displayTab, QString());
        cameraTab = new QWidget();
        cameraTab->setObjectName(QString::fromUtf8("cameraTab"));
        verticalLayout = new QVBoxLayout(cameraTab);
        verticalLayout->setObjectName(QString::fromUtf8("verticalLayout"));
        verticalSpacer_19 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_19);

        flachOnCheckBox = new QCheckBox(cameraTab);
        flachOnCheckBox->setObjectName(QString::fromUtf8("flachOnCheckBox"));

        verticalLayout->addWidget(flachOnCheckBox);

        verticalSpacer_20 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_20);

        horizontalLayout_6 = new QHBoxLayout();
        horizontalLayout_6->setObjectName(QString::fromUtf8("horizontalLayout_6"));
        label_6 = new QLabel(cameraTab);
        label_6->setObjectName(QString::fromUtf8("label_6"));

        horizontalLayout_6->addWidget(label_6);

        spinBox = new QSpinBox(cameraTab);
        spinBox->setObjectName(QString::fromUtf8("spinBox"));
        QSizePolicy sizePolicy5(QSizePolicy::Maximum, QSizePolicy::Fixed);
        sizePolicy5.setHorizontalStretch(0);
        sizePolicy5.setVerticalStretch(0);
        sizePolicy5.setHeightForWidth(spinBox->sizePolicy().hasHeightForWidth());
        spinBox->setSizePolicy(sizePolicy5);
        spinBox->setWrapping(false);
        spinBox->setFrame(false);
        spinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox->setAccelerated(true);
        spinBox->setMinimum(-100);
        spinBox->setMaximum(100);

        horizontalLayout_6->addWidget(spinBox);

        ContrastSlider = new QSlider(cameraTab);
        ContrastSlider->setObjectName(QString::fromUtf8("ContrastSlider"));
        ContrastSlider->setMinimum(-100);
        ContrastSlider->setMaximum(100);
        ContrastSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_6->addWidget(ContrastSlider);


        verticalLayout->addLayout(horizontalLayout_6);

        horizontalLayout_7 = new QHBoxLayout();
        horizontalLayout_7->setObjectName(QString::fromUtf8("horizontalLayout_7"));
        label_7 = new QLabel(cameraTab);
        label_7->setObjectName(QString::fromUtf8("label_7"));

        horizontalLayout_7->addWidget(label_7);

        spinBox_2 = new QSpinBox(cameraTab);
        spinBox_2->setObjectName(QString::fromUtf8("spinBox_2"));
        sizePolicy5.setHeightForWidth(spinBox_2->sizePolicy().hasHeightForWidth());
        spinBox_2->setSizePolicy(sizePolicy5);
        spinBox_2->setWrapping(false);
        spinBox_2->setFrame(false);
        spinBox_2->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_2->setMinimum(-100);
        spinBox_2->setMaximum(100);

        horizontalLayout_7->addWidget(spinBox_2);

        BrightnessSlider = new QSlider(cameraTab);
        BrightnessSlider->setObjectName(QString::fromUtf8("BrightnessSlider"));
        BrightnessSlider->setMinimum(-100);
        BrightnessSlider->setMaximum(100);
        BrightnessSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_7->addWidget(BrightnessSlider);


        verticalLayout->addLayout(horizontalLayout_7);

        verticalSpacer_21 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_21);

        horizontalLayout_14 = new QHBoxLayout();
        horizontalLayout_14->setObjectName(QString::fromUtf8("horizontalLayout_14"));
        label_4 = new QLabel(cameraTab);
        label_4->setObjectName(QString::fromUtf8("label_4"));

        horizontalLayout_14->addWidget(label_4);

        focusModeComboBox = new QComboBox(cameraTab);
        focusModeComboBox->setObjectName(QString::fromUtf8("focusModeComboBox"));
        QSizePolicy sizePolicy6(QSizePolicy::Expanding, QSizePolicy::Fixed);
        sizePolicy6.setHorizontalStretch(0);
        sizePolicy6.setVerticalStretch(0);
        sizePolicy6.setHeightForWidth(focusModeComboBox->sizePolicy().hasHeightForWidth());
        focusModeComboBox->setSizePolicy(sizePolicy6);

        horizontalLayout_14->addWidget(focusModeComboBox);


        verticalLayout->addLayout(horizontalLayout_14);

        verticalSpacer_22 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_22);

        horizontalLayout_9 = new QHBoxLayout();
        horizontalLayout_9->setObjectName(QString::fromUtf8("horizontalLayout_9"));
        label_9 = new QLabel(cameraTab);
        label_9->setObjectName(QString::fromUtf8("label_9"));

        horizontalLayout_9->addWidget(label_9);

        spinBox_3 = new QSpinBox(cameraTab);
        spinBox_3->setObjectName(QString::fromUtf8("spinBox_3"));
        sizePolicy5.setHeightForWidth(spinBox_3->sizePolicy().hasHeightForWidth());
        spinBox_3->setSizePolicy(sizePolicy5);
        spinBox_3->setWrapping(false);
        spinBox_3->setFrame(false);
        spinBox_3->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_3->setMinimum(-100);
        spinBox_3->setMaximum(100);

        horizontalLayout_9->addWidget(spinBox_3);

        zoomSlider = new QSlider(cameraTab);
        zoomSlider->setObjectName(QString::fromUtf8("zoomSlider"));
        zoomSlider->setMaximum(100);
        zoomSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_9->addWidget(zoomSlider);


        verticalLayout->addLayout(horizontalLayout_9);

        verticalSpacer_23 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_23);

        horizontalLayout_10 = new QHBoxLayout();
        horizontalLayout_10->setObjectName(QString::fromUtf8("horizontalLayout_10"));
        label_8 = new QLabel(cameraTab);
        label_8->setObjectName(QString::fromUtf8("label_8"));

        horizontalLayout_10->addWidget(label_8);

        cameraResolutionComboBox = new QComboBox(cameraTab);
        cameraResolutionComboBox->setObjectName(QString::fromUtf8("cameraResolutionComboBox"));
        sizePolicy6.setHeightForWidth(cameraResolutionComboBox->sizePolicy().hasHeightForWidth());
        cameraResolutionComboBox->setSizePolicy(sizePolicy6);

        horizontalLayout_10->addWidget(cameraResolutionComboBox);


        verticalLayout->addLayout(horizontalLayout_10);

        verticalSpacer_24 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout->addItem(verticalSpacer_24);

        tabWidget->addTab(cameraTab, QString());
        timersTab = new QWidget();
        timersTab->setObjectName(QString::fromUtf8("timersTab"));
        gridLayout_5 = new QGridLayout(timersTab);
        gridLayout_5->setObjectName(QString::fromUtf8("gridLayout_5"));
        horizontalLayout_11 = new QHBoxLayout();
        horizontalLayout_11->setSpacing(0);
        horizontalLayout_11->setObjectName(QString::fromUtf8("horizontalLayout_11"));
        timersScrollArea = new QScrollArea(timersTab);
        timersScrollArea->setObjectName(QString::fromUtf8("timersScrollArea"));
        timersScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        timersScrollArea->setWidgetResizable(true);
        timersScrollArea->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        scrollAreaWidgetContents = new QWidget();
        scrollAreaWidgetContents->setObjectName(QString::fromUtf8("scrollAreaWidgetContents"));
        scrollAreaWidgetContents->setGeometry(QRect(0, 0, 176, 204));
        gridLayout_3 = new QGridLayout(scrollAreaWidgetContents);
        gridLayout_3->setObjectName(QString::fromUtf8("gridLayout_3"));
        timersScrollArea->setWidget(scrollAreaWidgetContents);

        horizontalLayout_11->addWidget(timersScrollArea);


        gridLayout_5->addLayout(horizontalLayout_11, 0, 0, 1, 2);

        addTimeEdit = new QTimeEdit(timersTab);
        addTimeEdit->setObjectName(QString::fromUtf8("addTimeEdit"));

        gridLayout_5->addWidget(addTimeEdit, 1, 0, 1, 1);

        addTimeButton = new QPushButton(timersTab);
        addTimeButton->setObjectName(QString::fromUtf8("addTimeButton"));
        sizePolicy2.setHeightForWidth(addTimeButton->sizePolicy().hasHeightForWidth());
        addTimeButton->setSizePolicy(sizePolicy2);

        gridLayout_5->addWidget(addTimeButton, 1, 1, 1, 1);

        tabWidget->addTab(timersTab, QString());
        storageTab = new QWidget();
        storageTab->setObjectName(QString::fromUtf8("storageTab"));
        verticalLayout_5 = new QVBoxLayout(storageTab);
        verticalLayout_5->setObjectName(QString::fromUtf8("verticalLayout_5"));
        verticalSpacer_4 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_4);

        groupBox_2 = new QGroupBox(storageTab);
        groupBox_2->setObjectName(QString::fromUtf8("groupBox_2"));
        gridLayout_10 = new QGridLayout(groupBox_2);
        gridLayout_10->setObjectName(QString::fromUtf8("gridLayout_10"));
        horizontalLayout_12 = new QHBoxLayout();
        horizontalLayout_12->setObjectName(QString::fromUtf8("horizontalLayout_12"));
        saveToEdit = new QLineEdit(groupBox_2);
        saveToEdit->setObjectName(QString::fromUtf8("saveToEdit"));
        saveToEdit->setFocusPolicy(Qt::NoFocus);
        saveToEdit->setReadOnly(true);

        horizontalLayout_12->addWidget(saveToEdit);

        saveToButton = new QToolButton(groupBox_2);
        saveToButton->setObjectName(QString::fromUtf8("saveToButton"));

        horizontalLayout_12->addWidget(saveToButton);


        gridLayout_10->addLayout(horizontalLayout_12, 0, 0, 1, 1);


        verticalLayout_5->addWidget(groupBox_2);

        verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_3);

        groupBox_3 = new QGroupBox(storageTab);
        groupBox_3->setObjectName(QString::fromUtf8("groupBox_3"));
        gridLayout_11 = new QGridLayout(groupBox_3);
        gridLayout_11->setObjectName(QString::fromUtf8("gridLayout_11"));
        horizontalLayout_13 = new QHBoxLayout();
        horizontalLayout_13->setObjectName(QString::fromUtf8("horizontalLayout_13"));
        fileNamePrefixLineEdit = new QLineEdit(groupBox_3);
        fileNamePrefixLineEdit->setObjectName(QString::fromUtf8("fileNamePrefixLineEdit"));

        horizontalLayout_13->addWidget(fileNamePrefixLineEdit);

        label_14 = new QLabel(groupBox_3);
        label_14->setObjectName(QString::fromUtf8("label_14"));

        horizontalLayout_13->addWidget(label_14);


        gridLayout_11->addLayout(horizontalLayout_13, 0, 0, 1, 1);


        verticalLayout_5->addWidget(groupBox_3);

        verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_5->addItem(verticalSpacer_2);

        tabWidget->addTab(storageTab, QString());
        modefyTab = new QWidget();
        modefyTab->setObjectName(QString::fromUtf8("modefyTab"));
        verticalLayout_9 = new QVBoxLayout(modefyTab);
        verticalLayout_9->setObjectName(QString::fromUtf8("verticalLayout_9"));
        verticalSpacer = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer);

        modefyCheckBox = new QCheckBox(modefyTab);
        modefyCheckBox->setObjectName(QString::fromUtf8("modefyCheckBox"));
        modefyCheckBox->setEnabled(true);
        sizePolicy6.setHeightForWidth(modefyCheckBox->sizePolicy().hasHeightForWidth());
        modefyCheckBox->setSizePolicy(sizePolicy6);

        verticalLayout_9->addWidget(modefyCheckBox);

        verticalSpacer_6 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_6);

        horizontalLayout_17 = new QHBoxLayout();
        horizontalLayout_17->setSpacing(5);
        horizontalLayout_17->setObjectName(QString::fromUtf8("horizontalLayout_17"));
        label_19 = new QLabel(modefyTab);
        label_19->setObjectName(QString::fromUtf8("label_19"));
        sizePolicy1.setHeightForWidth(label_19->sizePolicy().hasHeightForWidth());
        label_19->setSizePolicy(sizePolicy1);

        horizontalLayout_17->addWidget(label_19);

        spinBox_4 = new QSpinBox(modefyTab);
        spinBox_4->setObjectName(QString::fromUtf8("spinBox_4"));
        spinBox_4->setEnabled(false);
        sizePolicy5.setHeightForWidth(spinBox_4->sizePolicy().hasHeightForWidth());
        spinBox_4->setSizePolicy(sizePolicy5);
        spinBox_4->setFrame(false);
        spinBox_4->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_4->setMinimum(-100);
        spinBox_4->setMaximum(100);

        horizontalLayout_17->addWidget(spinBox_4);

        modefyBrightnesSlider = new QSlider(modefyTab);
        modefyBrightnesSlider->setObjectName(QString::fromUtf8("modefyBrightnesSlider"));
        modefyBrightnesSlider->setEnabled(false);
        QSizePolicy sizePolicy7(QSizePolicy::Expanding, QSizePolicy::Preferred);
        sizePolicy7.setHorizontalStretch(0);
        sizePolicy7.setVerticalStretch(0);
        sizePolicy7.setHeightForWidth(modefyBrightnesSlider->sizePolicy().hasHeightForWidth());
        modefyBrightnesSlider->setSizePolicy(sizePolicy7);
        modefyBrightnesSlider->setMinimum(-100);
        modefyBrightnesSlider->setMaximum(100);
        modefyBrightnesSlider->setValue(0);
        modefyBrightnesSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_17->addWidget(modefyBrightnesSlider);


        verticalLayout_9->addLayout(horizontalLayout_17);

        horizontalLayout_16 = new QHBoxLayout();
        horizontalLayout_16->setSpacing(5);
        horizontalLayout_16->setObjectName(QString::fromUtf8("horizontalLayout_16"));
        horizontalLayout_16->setContentsMargins(-1, -1, -1, 0);
        label_18 = new QLabel(modefyTab);
        label_18->setObjectName(QString::fromUtf8("label_18"));
        sizePolicy1.setHeightForWidth(label_18->sizePolicy().hasHeightForWidth());
        label_18->setSizePolicy(sizePolicy1);

        horizontalLayout_16->addWidget(label_18);

        spinBox_5 = new QSpinBox(modefyTab);
        spinBox_5->setObjectName(QString::fromUtf8("spinBox_5"));
        spinBox_5->setEnabled(false);
        sizePolicy5.setHeightForWidth(spinBox_5->sizePolicy().hasHeightForWidth());
        spinBox_5->setSizePolicy(sizePolicy5);
        spinBox_5->setFrame(false);
        spinBox_5->setButtonSymbols(QAbstractSpinBox::NoButtons);
        spinBox_5->setMinimum(-100);
        spinBox_5->setMaximum(100);

        horizontalLayout_16->addWidget(spinBox_5);

        modefyContrastSlider = new QSlider(modefyTab);
        modefyContrastSlider->setObjectName(QString::fromUtf8("modefyContrastSlider"));
        modefyContrastSlider->setEnabled(false);
        sizePolicy7.setHeightForWidth(modefyContrastSlider->sizePolicy().hasHeightForWidth());
        modefyContrastSlider->setSizePolicy(sizePolicy7);
        modefyContrastSlider->setMinimum(-100);
        modefyContrastSlider->setMaximum(100);
        modefyContrastSlider->setSingleStep(10);
        modefyContrastSlider->setPageStep(10);
        modefyContrastSlider->setValue(0);
        modefyContrastSlider->setOrientation(Qt::Horizontal);

        horizontalLayout_16->addWidget(modefyContrastSlider);


        verticalLayout_9->addLayout(horizontalLayout_16);

        verticalSpacer_7 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_7);

        horizontalLayout_8 = new QHBoxLayout();
        horizontalLayout_8->setObjectName(QString::fromUtf8("horizontalLayout_8"));
        label_13 = new QLabel(modefyTab);
        label_13->setObjectName(QString::fromUtf8("label_13"));
        sizePolicy1.setHeightForWidth(label_13->sizePolicy().hasHeightForWidth());
        label_13->setSizePolicy(sizePolicy1);

        horizontalLayout_8->addWidget(label_13);

        resizeToWidthSpinBox = new QSpinBox(modefyTab);
        resizeToWidthSpinBox->setObjectName(QString::fromUtf8("resizeToWidthSpinBox"));
        resizeToWidthSpinBox->setEnabled(false);
        sizePolicy2.setHeightForWidth(resizeToWidthSpinBox->sizePolicy().hasHeightForWidth());
        resizeToWidthSpinBox->setSizePolicy(sizePolicy2);
        resizeToWidthSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        resizeToWidthSpinBox->setMinimum(1);
        resizeToWidthSpinBox->setMaximum(9999);

        horizontalLayout_8->addWidget(resizeToWidthSpinBox);

        label_15 = new QLabel(modefyTab);
        label_15->setObjectName(QString::fromUtf8("label_15"));
        QSizePolicy sizePolicy8(QSizePolicy::Preferred, QSizePolicy::Minimum);
        sizePolicy8.setHorizontalStretch(0);
        sizePolicy8.setVerticalStretch(0);
        sizePolicy8.setHeightForWidth(label_15->sizePolicy().hasHeightForWidth());
        label_15->setSizePolicy(sizePolicy8);

        horizontalLayout_8->addWidget(label_15);

        resizeToHeightSpinBox = new QSpinBox(modefyTab);
        resizeToHeightSpinBox->setObjectName(QString::fromUtf8("resizeToHeightSpinBox"));
        resizeToHeightSpinBox->setEnabled(false);
        sizePolicy2.setHeightForWidth(resizeToHeightSpinBox->sizePolicy().hasHeightForWidth());
        resizeToHeightSpinBox->setSizePolicy(sizePolicy2);
        resizeToHeightSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        resizeToHeightSpinBox->setMinimum(1);
        resizeToHeightSpinBox->setMaximum(9999);

        horizontalLayout_8->addWidget(resizeToHeightSpinBox);

        horizontalSpacer_7 = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_8->addItem(horizontalSpacer_7);


        verticalLayout_9->addLayout(horizontalLayout_8);

        horizontalLayout_15 = new QHBoxLayout();
        horizontalLayout_15->setObjectName(QString::fromUtf8("horizontalLayout_15"));
        label_5 = new QLabel(modefyTab);
        label_5->setObjectName(QString::fromUtf8("label_5"));

        horizontalLayout_15->addWidget(label_5);

        modefyQualityComboBox = new QComboBox(modefyTab);
        modefyQualityComboBox->setObjectName(QString::fromUtf8("modefyQualityComboBox"));
        modefyQualityComboBox->setEnabled(false);
        sizePolicy6.setHeightForWidth(modefyQualityComboBox->sizePolicy().hasHeightForWidth());
        modefyQualityComboBox->setSizePolicy(sizePolicy6);

        horizontalLayout_15->addWidget(modefyQualityComboBox);


        verticalLayout_9->addLayout(horizontalLayout_15);

        verticalSpacer_8 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_8);

        cropCheckBox = new QCheckBox(modefyTab);
        cropCheckBox->setObjectName(QString::fromUtf8("cropCheckBox"));
        cropCheckBox->setEnabled(false);

        verticalLayout_9->addWidget(cropCheckBox);

        horizontalLayout_18 = new QHBoxLayout();
        horizontalLayout_18->setObjectName(QString::fromUtf8("horizontalLayout_18"));
        horizontalSpacer_8 = new QSpacerItem(15, 0, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_18->addItem(horizontalSpacer_8);

        label_20 = new QLabel(modefyTab);
        label_20->setObjectName(QString::fromUtf8("label_20"));
        QSizePolicy sizePolicy9(QSizePolicy::Fixed, QSizePolicy::Preferred);
        sizePolicy9.setHorizontalStretch(0);
        sizePolicy9.setVerticalStretch(0);
        sizePolicy9.setHeightForWidth(label_20->sizePolicy().hasHeightForWidth());
        label_20->setSizePolicy(sizePolicy9);

        horizontalLayout_18->addWidget(label_20);

        cropStartXSpinBox = new QSpinBox(modefyTab);
        cropStartXSpinBox->setObjectName(QString::fromUtf8("cropStartXSpinBox"));
        cropStartXSpinBox->setEnabled(false);
        cropStartXSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        cropStartXSpinBox->setMaximum(9999);

        horizontalLayout_18->addWidget(cropStartXSpinBox);

        label_23 = new QLabel(modefyTab);
        label_23->setObjectName(QString::fromUtf8("label_23"));

        horizontalLayout_18->addWidget(label_23);

        cropStartYSpinBox = new QSpinBox(modefyTab);
        cropStartYSpinBox->setObjectName(QString::fromUtf8("cropStartYSpinBox"));
        cropStartYSpinBox->setEnabled(false);
        cropStartYSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        cropStartYSpinBox->setMaximum(9999);

        horizontalLayout_18->addWidget(cropStartYSpinBox);

        horizontalSpacer_11 = new QSpacerItem(13, 13, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_18->addItem(horizontalSpacer_11);


        verticalLayout_9->addLayout(horizontalLayout_18);

        horizontalLayout_19 = new QHBoxLayout();
        horizontalLayout_19->setObjectName(QString::fromUtf8("horizontalLayout_19"));
        horizontalSpacer_9 = new QSpacerItem(15, 0, QSizePolicy::Preferred, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(horizontalSpacer_9);

        label_22 = new QLabel(modefyTab);
        label_22->setObjectName(QString::fromUtf8("label_22"));
        sizePolicy9.setHeightForWidth(label_22->sizePolicy().hasHeightForWidth());
        label_22->setSizePolicy(sizePolicy9);

        horizontalLayout_19->addWidget(label_22);

        cropEndXSpinBox = new QSpinBox(modefyTab);
        cropEndXSpinBox->setObjectName(QString::fromUtf8("cropEndXSpinBox"));
        cropEndXSpinBox->setEnabled(false);
        cropEndXSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        cropEndXSpinBox->setMaximum(9999);

        horizontalLayout_19->addWidget(cropEndXSpinBox);

        label_70 = new QLabel(modefyTab);
        label_70->setObjectName(QString::fromUtf8("label_70"));

        horizontalLayout_19->addWidget(label_70);

        cropEndYSpinBox = new QSpinBox(modefyTab);
        cropEndYSpinBox->setObjectName(QString::fromUtf8("cropEndYSpinBox"));
        cropEndYSpinBox->setEnabled(false);
        cropEndYSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        cropEndYSpinBox->setMaximum(9999);

        horizontalLayout_19->addWidget(cropEndYSpinBox);

        horizontalSpacer_12 = new QSpacerItem(0, 0, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout_19->addItem(horizontalSpacer_12);


        verticalLayout_9->addLayout(horizontalLayout_19);

        verticalSpacer_9 = new QSpacerItem(0, 0, QSizePolicy::Minimum, QSizePolicy::Expanding);

        verticalLayout_9->addItem(verticalSpacer_9);

        tabWidget->addTab(modefyTab, QString());
        sendTab = new QWidget();
        sendTab->setObjectName(QString::fromUtf8("sendTab"));
        gridLayout_7 = new QGridLayout(sendTab);
        gridLayout_7->setObjectName(QString::fromUtf8("gridLayout_7"));
        sendMmsCheckBox = new QCheckBox(sendTab);
        sendMmsCheckBox->setObjectName(QString::fromUtf8("sendMmsCheckBox"));
        sizePolicy6.setHeightForWidth(sendMmsCheckBox->sizePolicy().hasHeightForWidth());
        sendMmsCheckBox->setSizePolicy(sizePolicy6);

        gridLayout_7->addWidget(sendMmsCheckBox, 0, 0, 1, 2);

        label_74 = new QLabel(sendTab);
        label_74->setObjectName(QString::fromUtf8("label_74"));

        gridLayout_7->addWidget(label_74, 1, 0, 1, 1);

        sendTolineEdit = new QLineEdit(sendTab);
        sendTolineEdit->setObjectName(QString::fromUtf8("sendTolineEdit"));
        sendTolineEdit->setEnabled(false);

        gridLayout_7->addWidget(sendTolineEdit, 1, 1, 1, 1);

        gridLayout_4 = new QGridLayout();
        gridLayout_4->setObjectName(QString::fromUtf8("gridLayout_4"));
        label_72 = new QLabel(sendTab);
        label_72->setObjectName(QString::fromUtf8("label_72"));

        gridLayout_4->addWidget(label_72, 0, 0, 1, 1);

        horizontalSpacer_13 = new QSpacerItem(0, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_13, 0, 1, 1, 1);

        label_73 = new QLabel(sendTab);
        label_73->setObjectName(QString::fromUtf8("label_73"));

        gridLayout_4->addWidget(label_73, 0, 2, 1, 1);

        sendScrollArea = new QScrollArea(sendTab);
        sendScrollArea->setObjectName(QString::fromUtf8("sendScrollArea"));
        sendScrollArea->setEnabled(false);
        sendScrollArea->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        sendScrollArea->setWidgetResizable(true);
        sendScrollArea->setAlignment(Qt::AlignLeading|Qt::AlignLeft|Qt::AlignTop);
        scrollAreaWidgetContents_4 = new QWidget();
        scrollAreaWidgetContents_4->setObjectName(QString::fromUtf8("scrollAreaWidgetContents_4"));
        scrollAreaWidgetContents_4->setGeometry(QRect(0, 0, 176, 126));
        sendScrollArea->setWidget(scrollAreaWidgetContents_4);

        gridLayout_4->addWidget(sendScrollArea, 1, 0, 1, 5);

        horizontalSpacer_5 = new QSpacerItem(5, 1, QSizePolicy::Preferred, QSizePolicy::Minimum);

        gridLayout_4->addItem(horizontalSpacer_5, 0, 3, 1, 1);

        label = new QLabel(sendTab);
        label->setObjectName(QString::fromUtf8("label"));

        gridLayout_4->addWidget(label, 0, 4, 1, 1);


        gridLayout_7->addLayout(gridLayout_4, 2, 0, 1, 2);

        horizontalLayout_52 = new QHBoxLayout();
        horizontalLayout_52->setObjectName(QString::fromUtf8("horizontalLayout_52"));
        addSendTimeEdit = new QTimeEdit(sendTab);
        addSendTimeEdit->setObjectName(QString::fromUtf8("addSendTimeEdit"));
        addSendTimeEdit->setEnabled(false);

        horizontalLayout_52->addWidget(addSendTimeEdit);

        sendImgCountSpinBox = new QSpinBox(sendTab);
        sendImgCountSpinBox->setObjectName(QString::fromUtf8("sendImgCountSpinBox"));
        sendImgCountSpinBox->setEnabled(false);
        sendImgCountSpinBox->setButtonSymbols(QAbstractSpinBox::NoButtons);
        sendImgCountSpinBox->setMinimum(1);
        sendImgCountSpinBox->setMaximum(999);
        sendImgCountSpinBox->setValue(10);

        horizontalLayout_52->addWidget(sendImgCountSpinBox);

        addSendTimeButton = new QPushButton(sendTab);
        addSendTimeButton->setObjectName(QString::fromUtf8("addSendTimeButton"));
        addSendTimeButton->setEnabled(false);
        QSizePolicy sizePolicy10(QSizePolicy::Ignored, QSizePolicy::Fixed);
        sizePolicy10.setHorizontalStretch(0);
        sizePolicy10.setVerticalStretch(0);
        sizePolicy10.setHeightForWidth(addSendTimeButton->sizePolicy().hasHeightForWidth());
        addSendTimeButton->setSizePolicy(sizePolicy10);

        horizontalLayout_52->addWidget(addSendTimeButton);


        gridLayout_7->addLayout(horizontalLayout_52, 3, 0, 1, 2);

        tabWidget->addTab(sendTab, QString());

        gridLayout->addWidget(tabWidget, 0, 0, 1, 1);

        stackedWidget->addWidget(page_2);
        page_3 = new QWidget();
        page_3->setObjectName(QString::fromUtf8("page_3"));
        verticalLayout_7 = new QVBoxLayout(page_3);
        verticalLayout_7->setObjectName(QString::fromUtf8("verticalLayout_7"));
        cameraFrame = new QFrame(page_3);
        cameraFrame->setObjectName(QString::fromUtf8("cameraFrame"));
        cameraFrame->setFrameShape(QFrame::StyledPanel);
        cameraFrame->setFrameShadow(QFrame::Raised);

        verticalLayout_7->addWidget(cameraFrame);

        stackedWidget->addWidget(page_3);

        verticalLayout_3->addWidget(stackedWidget);

#ifndef QT_NO_SHORTCUT
        label_2->setBuddy(displaySecBeforeSpinBox);
        label_3->setBuddy(displaySecDurationSpinBox);
        label_74->setBuddy(sendTolineEdit);
#endif // QT_NO_SHORTCUT

        retranslateUi(SettingsDialog);
        QObject::connect(turnOnDisplayCheckBox, SIGNAL(toggled(bool)), displaySecBeforeSpinBox, SLOT(setEnabled(bool)));
        QObject::connect(modefyCheckBox, SIGNAL(toggled(bool)), modefyBrightnesSlider, SLOT(setEnabled(bool)));
        QObject::connect(modefyCheckBox, SIGNAL(toggled(bool)), modefyContrastSlider, SLOT(setEnabled(bool)));
        QObject::connect(modefyCheckBox, SIGNAL(toggled(bool)), resizeToWidthSpinBox, SLOT(setEnabled(bool)));
        QObject::connect(modefyCheckBox, SIGNAL(toggled(bool)), resizeToHeightSpinBox, SLOT(setEnabled(bool)));
        QObject::connect(sendMmsCheckBox, SIGNAL(toggled(bool)), addSendTimeEdit, SLOT(setEnabled(bool)));
        QObject::connect(sendMmsCheckBox, SIGNAL(toggled(bool)), addSendTimeButton, SLOT(setEnabled(bool)));
        QObject::connect(sendMmsCheckBox, SIGNAL(toggled(bool)), sendTolineEdit, SLOT(setEnabled(bool)));
        QObject::connect(sendMmsCheckBox, SIGNAL(toggled(bool)), sendScrollArea, SLOT(setEnabled(bool)));
        QObject::connect(turnOnDisplayCheckBox, SIGNAL(toggled(bool)), displaySecDurationSpinBox, SLOT(setEnabled(bool)));
        QObject::connect(spinBox, SIGNAL(valueChanged(int)), ContrastSlider, SLOT(setValue(int)));
        QObject::connect(spinBox_2, SIGNAL(valueChanged(int)), BrightnessSlider, SLOT(setValue(int)));
        QObject::connect(spinBox_3, SIGNAL(valueChanged(int)), zoomSlider, SLOT(setValue(int)));
        QObject::connect(ContrastSlider, SIGNAL(valueChanged(int)), spinBox, SLOT(setValue(int)));
        QObject::connect(BrightnessSlider, SIGNAL(valueChanged(int)), spinBox_2, SLOT(setValue(int)));
        QObject::connect(zoomSlider, SIGNAL(valueChanged(int)), spinBox_3, SLOT(setValue(int)));
        QObject::connect(spinBox_4, SIGNAL(valueChanged(int)), modefyBrightnesSlider, SLOT(setValue(int)));
        QObject::connect(spinBox_5, SIGNAL(valueChanged(int)), modefyContrastSlider, SLOT(setValue(int)));
        QObject::connect(modefyContrastSlider, SIGNAL(valueChanged(int)), spinBox_5, SLOT(setValue(int)));
        QObject::connect(modefyBrightnesSlider, SIGNAL(valueChanged(int)), spinBox_4, SLOT(setValue(int)));
        QObject::connect(modefyCheckBox, SIGNAL(toggled(bool)), spinBox_4, SLOT(setEnabled(bool)));
        QObject::connect(modefyCheckBox, SIGNAL(toggled(bool)), spinBox_5, SLOT(setEnabled(bool)));
        QObject::connect(modefyCheckBox, SIGNAL(toggled(bool)), spinBox_4, SLOT(setEnabled(bool)));
        QObject::connect(sendMmsCheckBox, SIGNAL(toggled(bool)), sendImgCountSpinBox, SLOT(setEnabled(bool)));
        QObject::connect(modefyCheckBox, SIGNAL(toggled(bool)), modefyQualityComboBox, SLOT(setEnabled(bool)));
        QObject::connect(cropCheckBox, SIGNAL(toggled(bool)), cropStartXSpinBox, SLOT(setEnabled(bool)));
        QObject::connect(cropCheckBox, SIGNAL(toggled(bool)), cropStartYSpinBox, SLOT(setEnabled(bool)));
        QObject::connect(cropCheckBox, SIGNAL(toggled(bool)), cropEndXSpinBox, SLOT(setEnabled(bool)));
        QObject::connect(cropCheckBox, SIGNAL(toggled(bool)), cropEndYSpinBox, SLOT(setEnabled(bool)));
        QObject::connect(modefyCheckBox, SIGNAL(toggled(bool)), cropCheckBox, SLOT(setEnabled(bool)));
        QObject::connect(sendMmsCheckBox, SIGNAL(toggled(bool)), sendImgCountSpinBox, SLOT(setEnabled(bool)));

        stackedWidget->setCurrentIndex(1);
        tabWidget->setCurrentIndex(5);


        QMetaObject::connectSlotsByName(SettingsDialog);
    } // setupUi

    void retranslateUi(QDialog *SettingsDialog)
    {
        SettingsDialog->setWindowTitle(QApplication::translate("SettingsDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        displayButton->setText(QApplication::translate("SettingsDialog", "Display", 0, QApplication::UnicodeUTF8));
        cameraButton->setText(QApplication::translate("SettingsDialog", "Camera", 0, QApplication::UnicodeUTF8));
        TimersButton->setText(QApplication::translate("SettingsDialog", "Timers", 0, QApplication::UnicodeUTF8));
        storageButton->setText(QApplication::translate("SettingsDialog", "Storage", 0, QApplication::UnicodeUTF8));
        modefyButton->setText(QApplication::translate("SettingsDialog", "Image Modifications", 0, QApplication::UnicodeUTF8));
        sendButton->setText(QApplication::translate("SettingsDialog", "Send Options", 0, QApplication::UnicodeUTF8));
        groupBox_4->setTitle(QString());
        turnOnDisplayCheckBox->setText(QApplication::translate("SettingsDialog", "Camera preview", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("SettingsDialog", "Sec before capture", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("SettingsDialog", "Sec duration", 0, QApplication::UnicodeUTF8));
        powerSaveGroupBox->setTitle(QApplication::translate("SettingsDialog", "Power save", 0, QApplication::UnicodeUTF8));
        turnOfGsmCheckBox->setText(QApplication::translate("SettingsDialog", "Turn off network", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(displayTab), QApplication::translate("SettingsDialog", "Display", 0, QApplication::UnicodeUTF8));
        flachOnCheckBox->setText(QApplication::translate("SettingsDialog", "Flash On", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("SettingsDialog", "Contrast", 0, QApplication::UnicodeUTF8));
        label_7->setText(QApplication::translate("SettingsDialog", "Brightness", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("SettingsDialog", "Focus mode", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("SettingsDialog", "Zoom", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("SettingsDialog", "Resolution", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(cameraTab), QApplication::translate("SettingsDialog", "Camera", 0, QApplication::UnicodeUTF8));
        addTimeEdit->setDisplayFormat(QApplication::translate("SettingsDialog", "h:mm AP", 0, QApplication::UnicodeUTF8));
        addTimeButton->setText(QApplication::translate("SettingsDialog", "Add", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(timersTab), QApplication::translate("SettingsDialog", "Timers", 0, QApplication::UnicodeUTF8));
        groupBox_2->setTitle(QApplication::translate("SettingsDialog", "Save to folder", 0, QApplication::UnicodeUTF8));
        saveToButton->setText(QApplication::translate("SettingsDialog", "...", 0, QApplication::UnicodeUTF8));
        groupBox_3->setTitle(QApplication::translate("SettingsDialog", "File name", 0, QApplication::UnicodeUTF8));
        label_14->setText(QApplication::translate("SettingsDialog", "_YYYYMMDDHHMMSS", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(storageTab), QApplication::translate("SettingsDialog", "Storage", 0, QApplication::UnicodeUTF8));
        modefyCheckBox->setText(QApplication::translate("SettingsDialog", "Modify images before sending", 0, QApplication::UnicodeUTF8));
        label_19->setText(QApplication::translate("SettingsDialog", "Brightnes", 0, QApplication::UnicodeUTF8));
        label_18->setText(QApplication::translate("SettingsDialog", "Contrast", 0, QApplication::UnicodeUTF8));
        label_13->setText(QApplication::translate("SettingsDialog", "Resize to", 0, QApplication::UnicodeUTF8));
        label_15->setText(QApplication::translate("SettingsDialog", "X", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("SettingsDialog", "Quality", 0, QApplication::UnicodeUTF8));
        cropCheckBox->setText(QApplication::translate("SettingsDialog", "Crop", 0, QApplication::UnicodeUTF8));
        label_20->setText(QApplication::translate("SettingsDialog", "Start", 0, QApplication::UnicodeUTF8));
        label_23->setText(QApplication::translate("SettingsDialog", ",", 0, QApplication::UnicodeUTF8));
        label_22->setText(QApplication::translate("SettingsDialog", "End  ", 0, QApplication::UnicodeUTF8));
        label_70->setText(QApplication::translate("SettingsDialog", ",", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(modefyTab), QApplication::translate("SettingsDialog", "Modify", 0, QApplication::UnicodeUTF8));
        sendMmsCheckBox->setText(QApplication::translate("SettingsDialog", "Send Images via MMS", 0, QApplication::UnicodeUTF8));
        label_74->setText(QApplication::translate("SettingsDialog", "Send To:", 0, QApplication::UnicodeUTF8));
        label_72->setText(QApplication::translate("SettingsDialog", "Time", 0, QApplication::UnicodeUTF8));
        label_73->setText(QApplication::translate("SettingsDialog", "Images", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("SettingsDialog", "Delete", 0, QApplication::UnicodeUTF8));
        addSendTimeEdit->setDisplayFormat(QApplication::translate("SettingsDialog", "h:mm AP", 0, QApplication::UnicodeUTF8));
        addSendTimeButton->setText(QApplication::translate("SettingsDialog", "Add", 0, QApplication::UnicodeUTF8));
        tabWidget->setTabText(tabWidget->indexOf(sendTab), QApplication::translate("SettingsDialog", "Send", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SettingsDialog: public Ui_SettingsDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGSDIALOG_H
