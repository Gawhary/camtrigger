/********************************************************************************
** Form generated from reading UI file 'settingsmaindialog.ui'
**
** Created: Wed Feb 9 23:30:33 2011
**      by: Qt User Interface Compiler version 4.6.3
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_SETTINGSMAINDIALOG_H
#define UI_SETTINGSMAINDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QStackedWidget>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_SettingsMainDialog
{
public:
    QStackedWidget *stackedWidget;
    QWidget *page;
    QPushButton *pushButton_3;
    QPushButton *pushButton_2;
    QPushButton *pushButton_4;
    QPushButton *displayButton;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QWidget *page_2;

    void setupUi(QDialog *SettingsMainDialog)
    {
        if (SettingsMainDialog->objectName().isEmpty())
            SettingsMainDialog->setObjectName(QString::fromUtf8("SettingsMainDialog"));
        SettingsMainDialog->resize(429, 487);
        stackedWidget = new QStackedWidget(SettingsMainDialog);
        stackedWidget->setObjectName(QString::fromUtf8("stackedWidget"));
        stackedWidget->setGeometry(QRect(60, 70, 351, 321));
        page = new QWidget();
        page->setObjectName(QString::fromUtf8("page"));
        pushButton_3 = new QPushButton(page);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setGeometry(QRect(10, 140, 175, 23));
        pushButton_2 = new QPushButton(page);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(10, 98, 175, 23));
        pushButton_4 = new QPushButton(page);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setGeometry(QRect(10, 182, 175, 23));
        displayButton = new QPushButton(page);
        displayButton->setObjectName(QString::fromUtf8("displayButton"));
        displayButton->setGeometry(QRect(10, 56, 175, 23));
        pushButton_5 = new QPushButton(page);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setGeometry(QRect(10, 224, 175, 23));
        pushButton_6 = new QPushButton(page);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setGeometry(QRect(10, 266, 175, 23));
        stackedWidget->addWidget(page);
        page_2 = new QWidget();
        page_2->setObjectName(QString::fromUtf8("page_2"));
        stackedWidget->addWidget(page_2);

        retranslateUi(SettingsMainDialog);

        QMetaObject::connectSlotsByName(SettingsMainDialog);
    } // setupUi

    void retranslateUi(QDialog *SettingsMainDialog)
    {
        SettingsMainDialog->setWindowTitle(QApplication::translate("SettingsMainDialog", "Dialog", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("SettingsMainDialog", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("SettingsMainDialog", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_4->setText(QApplication::translate("SettingsMainDialog", "PushButton", 0, QApplication::UnicodeUTF8));
        displayButton->setText(QApplication::translate("SettingsMainDialog", "Display", 0, QApplication::UnicodeUTF8));
        pushButton_5->setText(QApplication::translate("SettingsMainDialog", "PushButton", 0, QApplication::UnicodeUTF8));
        pushButton_6->setText(QApplication::translate("SettingsMainDialog", "PushButton", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class SettingsMainDialog: public Ui_SettingsMainDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_SETTINGSMAINDIALOG_H
