/********************************************************************************
** Form generated from reading UI file 'timeentity.ui'
**
** Created: Fri Mar 4 01:45:55 2011
**      by: Qt User Interface Compiler version 4.7.1
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_TIMEENTITY_H
#define UI_TIMEENTITY_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHBoxLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLCDNumber>
#include <QtGui/QToolButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_TimeEntity
{
public:
    QHBoxLayout *horizontalLayout;
    QLCDNumber *lcdNumber;
    QToolButton *toolButton;

    void setupUi(QWidget *TimeEntity)
    {
        if (TimeEntity->objectName().isEmpty())
            TimeEntity->setObjectName(QString::fromUtf8("TimeEntity"));
        TimeEntity->resize(119, 23);
        QSizePolicy sizePolicy(QSizePolicy::MinimumExpanding, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(TimeEntity->sizePolicy().hasHeightForWidth());
        TimeEntity->setSizePolicy(sizePolicy);
        TimeEntity->setLayoutDirection(Qt::LeftToRight);
        horizontalLayout = new QHBoxLayout(TimeEntity);
        horizontalLayout->setSpacing(0);
        horizontalLayout->setContentsMargins(0, 0, 0, 0);
        horizontalLayout->setObjectName(QString::fromUtf8("horizontalLayout"));
        lcdNumber = new QLCDNumber(TimeEntity);
        lcdNumber->setObjectName(QString::fromUtf8("lcdNumber"));
        QSizePolicy sizePolicy1(QSizePolicy::MinimumExpanding, QSizePolicy::MinimumExpanding);
        sizePolicy1.setHorizontalStretch(0);
        sizePolicy1.setVerticalStretch(0);
        sizePolicy1.setHeightForWidth(lcdNumber->sizePolicy().hasHeightForWidth());
        lcdNumber->setSizePolicy(sizePolicy1);
        lcdNumber->setNumDigits(5);
        lcdNumber->setDigitCount(5);

        horizontalLayout->addWidget(lcdNumber);

        toolButton = new QToolButton(TimeEntity);
        toolButton->setObjectName(QString::fromUtf8("toolButton"));
        QSizePolicy sizePolicy2(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy2.setHorizontalStretch(0);
        sizePolicy2.setVerticalStretch(0);
        sizePolicy2.setHeightForWidth(toolButton->sizePolicy().hasHeightForWidth());
        toolButton->setSizePolicy(sizePolicy2);

        horizontalLayout->addWidget(toolButton);


        retranslateUi(TimeEntity);
        QObject::connect(toolButton, SIGNAL(clicked()), TimeEntity, SLOT(close()));

        QMetaObject::connectSlotsByName(TimeEntity);
    } // setupUi

    void retranslateUi(QWidget *TimeEntity)
    {
        TimeEntity->setWindowTitle(QApplication::translate("TimeEntity", "Form", 0, QApplication::UnicodeUTF8));
        toolButton->setText(QApplication::translate("TimeEntity", "X", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class TimeEntity: public Ui_TimeEntity {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_TIMEENTITY_H
