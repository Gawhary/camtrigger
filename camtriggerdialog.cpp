#include "camtriggerdialog.h"
#include "ui_settingsdialog.h"
#include "timeentity.h"
#include <QMessageBox>
#include <QScrollBar>
#include <QFileDialog>
#include "sendentity.h"
#include <QDebug>

//for turn on backlight
#include <HAL.h>
//#include <hal_data.h>
#include <e32std.h>

CamTriggerDialog::CamTriggerDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDialog)
{
    ui->setupUi(this);

    ui->sendTolineEdit->setInputMethodHints(Qt::ImhPreferNumbers);

#ifdef SELFSIGNED
    ui->powerSaveGroupBox->hide();
#endif
    
    //TODO: set ui initial states
    
    
    
    m_camTrigger = new CamTrigger(this);

    m_ExitAction = new QAction(QString(tr("Exit")), this);
    m_okAction = new QAction(QString(tr("OK")), this);
    m_cancelAction = new QAction(QString(tr("Cancel")), this);
    m_startTrigger = new QAction(QString(tr("Start Trigger")),this);
    m_stopTrigger = new QAction(QString(tr("Stop Trigger")),this);

    m_ExitAction->setSoftKeyRole(QAction::NegativeSoftKey);
    m_okAction->setSoftKeyRole(QAction::PositiveSoftKey);
    m_cancelAction->setSoftKeyRole(QAction::NegativeSoftKey);
    m_startTrigger->setSoftKeyRole(QAction::PositiveSoftKey);
    m_stopTrigger->setSoftKeyRole(QAction::PositiveSoftKey);

    connect(m_ExitAction, SIGNAL(triggered()), this, SLOT(onExitAction()));
    connect(m_okAction, SIGNAL(triggered()), this, SLOT(saveUI()));
    connect(m_okAction, SIGNAL(triggered()), this, SLOT(backToMainPage()));
    connect(m_cancelAction, SIGNAL(triggered()), this, SLOT(backToMainPage()));
    connect(m_cancelAction, SIGNAL(triggered()), this, SLOT(loadUI()));
    connect(m_startTrigger, SIGNAL(triggered()), this, SLOT(onStartTriggerAction()));
    connect(m_stopTrigger, SIGNAL(triggered()), this, SLOT(onStopTriggerAction()));

    connect(m_camTrigger, SIGNAL(turnOnCameraPreview()), this, SLOT(onTurnOnCameraPreview()));
    connect(m_camTrigger, SIGNAL(turnOffCameraPreview()), this, SLOT(onTurnOffCameraPreview()));

    addAction(m_ExitAction);
    addAction(m_startTrigger);

    ui->stackedWidget->setCurrentIndex(0);

    m_timersScrollWidget = new QWidget(ui->timersScrollArea);
    ui->timersScrollArea->setWidget(m_timersScrollWidget);
    m_timersLayout = new QGridLayout(m_timersScrollWidget);
    m_timersScrollWidget->setLayout(m_timersLayout);
    ui->timersScrollArea->setFocusProxy(m_timersScrollWidget);

    m_sendTimersScrollWidget = new QWidget(ui->sendScrollArea);
    ui->sendScrollArea->setWidget(m_sendTimersScrollWidget);
    m_sendTimersLayout = new QGridLayout(m_sendTimersScrollWidget);
    m_sendTimersScrollWidget->setLayout(m_sendTimersLayout);
    ui->sendScrollArea->setFocusProxy(m_sendTimersScrollWidget);

    m_viewFinder = new QCameraViewfinder(ui->cameraFrame);
    QGridLayout *cameraFrameLayout = new QGridLayout(ui->cameraFrame);
    ui->cameraFrame->setLayout(cameraFrameLayout);
    cameraFrameLayout->addWidget(m_viewFinder);
    //m_viewFinder->showMaximized();


    //enable/disable flash checkbox based on camera support
    ui->flachOnCheckBox->setEnabled(m_camTrigger->isFlashSupported());

    //fill camera resolutions compobox
    QList<QSize> resolutions = m_camTrigger->supportedResolutions();
    foreach (QSize resolution, resolutions) {
        QString resolutionString = QString("%1 x %2").arg(resolution.width()).arg(resolution.height());
        ui->cameraResolutionComboBox->addItem(resolutionString, resolution);
    }
    //fill camera focus mode compobox
    QMap<QString, int> focusModes = m_camTrigger->supportedFocusModes();
    for(QMap<QString, int>::iterator i = focusModes.begin(); i != focusModes.end(); i++) {
        ui->focusModeComboBox->addItem(i.key(),i.value());
    }

    //fill modefy quality compobox
    ui->modefyQualityComboBox->clear();
    ui->modefyQualityComboBox->addItem(tr("Very Low"),0);
    ui->modefyQualityComboBox->addItem(tr("Low"),33);
    ui->modefyQualityComboBox->addItem(tr("Normal"),-1);//default
    ui->modefyQualityComboBox->addItem(tr("High"),66);
    ui->modefyQualityComboBox->addItem(tr("Very High"),100);


    m_camTrigger->loadSettings();
    loadUI();
//    test();//TODO: remove


    turnOnBacklightTimer = new QTimer(this);
    turnOnBacklightTimer->setInterval(1000);
    connect(turnOnBacklightTimer, SIGNAL(timeout()), this, SLOT(turnOnBacklight()));

}

CamTriggerDialog::~CamTriggerDialog()
{
    delete ui;
}

void CamTriggerDialog::on_displayButton_clicked()
{
    goToSettingsPage(0);
}
void CamTriggerDialog::on_cameraButton_clicked()
{
    goToSettingsPage(1);
}

void CamTriggerDialog::on_TimersButton_clicked()
{
    goToSettingsPage(2);
}

void CamTriggerDialog::on_storageButton_clicked()
{
    goToSettingsPage(3);
}

void CamTriggerDialog::on_modefyButton_clicked()
{
    goToSettingsPage(4);
}


void CamTriggerDialog::on_sendButton_clicked()
{
    goToSettingsPage(5);
}

void CamTriggerDialog::goToSettingsPage(int index){
    //go to settings stacked widget
    ui->stackedWidget->setCurrentIndex(1);
    //change soft keys
    removeAction(m_startTrigger);
    removeAction(m_ExitAction);
    addAction(m_okAction);
    addAction(m_cancelAction);
    //go to indexed tab page
    ui->tabWidget->setCurrentIndex(index);

}

void CamTriggerDialog::backToMainPage(){
    //back to main stacked widget
    ui->stackedWidget->setCurrentIndex(0);
    //change soft keys
    removeAction(m_okAction);
    removeAction(m_cancelAction);
    addAction(m_startTrigger);
    addAction(m_ExitAction);

}

void CamTriggerDialog::saveUI(){
    if(!m_camTrigger)
        return;
    //Display
    bool previewEnabled = ui->turnOnDisplayCheckBox->isChecked();
    if(previewEnabled)
        m_camTrigger->setCameraView(m_viewFinder);
    else
        m_camTrigger->setCameraView(NULL);
    m_camTrigger->enablePreview(previewEnabled);
    m_camTrigger->setPreviewSecBeforeCapture( ui->displaySecBeforeSpinBox->value());
    m_camTrigger->setPreviewDuration( ui->displaySecDurationSpinBox->value());
//    m_camTrigger->enableTurnOffDisplay(ui->powerSaveCheckBox->isChecked());
    m_camTrigger->enableTurnOffNetwork(ui->turnOfGsmCheckBox->isChecked());
    //Camera
    m_camTrigger->enableFlash(ui->flachOnCheckBox->isChecked());
    m_camTrigger->setContrast(ui->ContrastSlider->value());
    m_camTrigger->setBrightnes(ui->BrightnessSlider->value());
//    m_camTrigger->enableAutoFocus(ui->AutoFocusRadioButton->isChecked());
    m_camTrigger->setFocusMode(ui->focusModeComboBox->itemData(ui->focusModeComboBox->currentIndex()).toInt());
//    m_camTrigger->setFocus(qreal(ui->focusSlider->value())/qreal(ui->focusSlider->maximum()));
    m_camTrigger->setZoom(qreal(ui->zoomSlider->value())/qreal(ui->zoomSlider->maximum()));
    m_camTrigger->setResolution(ui->cameraResolutionComboBox->itemData(ui->cameraResolutionComboBox->currentIndex()).toSize());
    //Storage
    m_camTrigger->setFileNamePrefix( ui->fileNamePrefixLineEdit->text());
    m_camTrigger->setDirectory(ui->saveToEdit->text());
    //Modefy
    m_camTrigger->enableModefy(ui->modefyCheckBox->isChecked());
    m_camTrigger->setModefyContrast(ui->modefyContrastSlider->value());
    m_camTrigger->setModefyBrightnes(ui->modefyBrightnesSlider->value());
    m_camTrigger->setModefyResolution(QSize(ui->resizeToWidthSpinBox->value(),ui->resizeToHeightSpinBox->value()));
    m_camTrigger->setModefyQuality(ui->modefyQualityComboBox->itemData(ui->modefyQualityComboBox->currentIndex()).toInt());
    m_camTrigger->enableCrop(ui->cropCheckBox->isChecked());
    m_camTrigger->setModefyCropStart(QPoint(ui->cropStartXSpinBox->value(),ui->cropStartYSpinBox->value()));
    m_camTrigger->setModefyCropEnd(QPoint(ui->cropEndXSpinBox->value(), ui->cropEndYSpinBox->value()));

    //Capture Timers
    m_camTrigger->clearCaptureTimers();
    QObjectList captureTimers = m_timersScrollWidget->children();
    foreach (QObject *child, captureTimers) {
        TimeEntity *timer = dynamic_cast<TimeEntity*>(child);
        if(timer){
            m_camTrigger->addCaptureTimer(timer->time());
        }
    }

    //Send
    m_camTrigger->clearSendTimers();
    m_camTrigger->enableSendMms( ui->sendMmsCheckBox->isChecked());
    m_camTrigger->setMmsDstenation( ui->sendTolineEdit->text());
    QObjectList sendTimers = m_sendTimersScrollWidget->children();
    foreach (QObject *child, sendTimers) {
        SendEntity *timer = dynamic_cast<SendEntity*>(child);
        if(timer){
            m_camTrigger->addSendTimer(timer->time(), timer->imgCount());
        }
    }
    m_camTrigger->saveSettings();
}

void CamTriggerDialog::test(){
    qDebug() << "Fill test data:";
    int captureTimers = 10;
    int senTimers = 3;
    QTime nextCaptureTime = QTime::currentTime();
    for(int c = 0; c < captureTimers; c++){
        nextCaptureTime = nextCaptureTime.addSecs(60+(qrand()%120));
        ui->addTimeEdit->setTime(nextCaptureTime);
        on_addTimeButton_clicked();
        qDebug() << "Capture time added: " << nextCaptureTime.toString();
    }
    ui->sendMmsCheckBox->setChecked(false);
    QTime nextSendTime = nextCaptureTime;
    for(int s = 0; s < senTimers; s++){
        nextSendTime = nextSendTime.addSecs(60+(qrand()%120));
        ui->addSendTimeEdit->setTime(nextSendTime);
        on_addSendTimeButton_clicked();
        int imgCount = qrand()%10;
        QObjectList sendTimers = m_sendTimersScrollWidget->children();
        foreach (QObject *child, sendTimers) {
            SendEntity *timer = dynamic_cast<SendEntity*>(child);
            if(timer){
                if(timer->time() == nextSendTime)
                {
                    timer->setImgCount(imgCount);
                }
            }
        }
        qDebug() << "Send time added: " << nextSendTime.toString() << " ,image count: " << imgCount;
    }

    ui->turnOnDisplayCheckBox->setChecked(false);
    ui->displaySecBeforeSpinBox->setValue(30);
    ui->displaySecDurationSpinBox->setValue(45);
//    ui->flachOnCheckBox->setChecked(true);

    saveUI();
//    onStartTriggerAction();

}

void CamTriggerDialog::loadUI()
{
    if(!m_camTrigger)
        return;
    //Display
    ui->turnOnDisplayCheckBox->setChecked(m_camTrigger->isPreviewEnabled());
    ui->displaySecBeforeSpinBox->setValue(m_camTrigger->previewSecBeforeCapture());
    ui->displaySecDurationSpinBox->setValue(m_camTrigger->previewDuration());
//    ui->powerSaveCheckBox->setChecked(m_camTrigger->isTurnOffDisplayEnabled());
    ui->turnOfGsmCheckBox->setChecked(m_camTrigger->isTurnOffNetworkEnabled());
    //Camera
    ui->flachOnCheckBox->setChecked(m_camTrigger->isFlashEnabled());
    ui->ContrastSlider->setValue(m_camTrigger->contrast());
    ui->BrightnessSlider->setValue(m_camTrigger->brightnes());
//    ui->AutoFocusRadioButton->setChecked(m_camTrigger->isAutoFocusEnabled());
    ui->focusModeComboBox->setCurrentIndex(ui->focusModeComboBox->findData(m_camTrigger->focusMode()));
//    ui->focusSlider->setValue(m_camTrigger->focus()*ui->focusSlider->maximum());
    ui->zoomSlider->setValue(m_camTrigger->zoom()*ui->zoomSlider->maximum());
    ui->cameraResolutionComboBox->setCurrentIndex(ui->cameraResolutionComboBox->findData(m_camTrigger->resolution()));
    //Storage
    ui->fileNamePrefixLineEdit->setText(m_camTrigger->fileNamePrefix());
    ui->saveToEdit->setText(m_camTrigger->directory());
    //Modefy
    ui->modefyCheckBox->setChecked(m_camTrigger->isModefyEnabled());
    ui->modefyContrastSlider->setValue(m_camTrigger->modefyContrast());
    ui->modefyBrightnesSlider->setValue(m_camTrigger->modefyBrightnes());
    ui->resizeToWidthSpinBox->setValue(m_camTrigger->modefyResolution().width());
    ui->resizeToHeightSpinBox->setValue(m_camTrigger->modefyResolution().height());
    ui->modefyQualityComboBox->setCurrentIndex(ui->modefyQualityComboBox->findData(m_camTrigger->modefyQuality()));
    ui->cropCheckBox->setChecked(m_camTrigger->isCropEnabled());
    ui->cropStartXSpinBox->setValue(m_camTrigger->modefyCropStart().x());
    ui->cropStartYSpinBox->setValue(m_camTrigger->modefyCropStart().y());
    ui->cropEndXSpinBox->setValue(m_camTrigger->modefyCropEnd().x());
    ui->cropEndYSpinBox->setValue(m_camTrigger->modefyCropEnd().y());

    //Capture Timers
    QObjectList existingCaptureTimers = m_timersScrollWidget->children();
    foreach (QObject *child, existingCaptureTimers) {
        TimeEntity *timer = dynamic_cast<TimeEntity*>(child);
        if(timer){
            delete timer;
        }
    }
    QList<QTime> captureTimers = m_camTrigger->captureTimers();
     for (QList<QTime>::iterator i = captureTimers.begin(); i != captureTimers.end(); i++){
        addCaptureTimer(*i);
    }

    //Send
    ui->sendMmsCheckBox->setChecked(m_camTrigger->isSendMmsEnabled());
    ui->sendTolineEdit->setText(m_camTrigger->mmsDistenation());

    QObjectList existingSendTimers = m_sendTimersScrollWidget->children();
    foreach (QObject *child, existingSendTimers) {
        SendEntity *timer = dynamic_cast<SendEntity*>(child);
        if(timer){
            delete timer;
        }
    }
    QMap<QTime,int> sendTimers = m_camTrigger->sendTimers();
    for (QMap<QTime,int>::iterator i = sendTimers.begin(); i != sendTimers.end(); i++) {
        addSendTimer(i.key(), i.value());
    }

    //set crop spinboxes maximum and maxmum values
    QSize currentResolution(ui->cameraResolutionComboBox->itemData(
                                ui->cameraResolutionComboBox->currentIndex()).toSize());
    if(currentResolution.width() < ui->cropEndXSpinBox->value())
        ui->cropStartXSpinBox->setMaximum(currentResolution.width()-1);
    else
        ui->cropStartXSpinBox->setMaximum(ui->cropEndXSpinBox->value()-1);

    if(currentResolution.height() < ui->cropEndYSpinBox->value())
        ui->cropStartYSpinBox->setMaximum(currentResolution.height()-1);
    else
        ui->cropStartYSpinBox->setMaximum(ui->cropEndYSpinBox->value()-1);
    ui->cropEndXSpinBox->setMinimum(ui->cropStartXSpinBox->value()+1);
    ui->cropEndYSpinBox->setMinimum(ui->cropStartYSpinBox->value()+1);
    ui->cropEndXSpinBox->setMaximum(currentResolution.width());
    ui->cropEndYSpinBox->setMaximum(currentResolution.height());
}
void CamTriggerDialog::onStartTriggerAction(){
    ui->stackedWidget->setCurrentIndex(2);
    removeAction(m_startTrigger);
    addAction(m_stopTrigger);

    m_camTrigger->start();
}
void CamTriggerDialog::onStopTriggerAction(){
    ui->stackedWidget->setCurrentIndex(0);
    removeAction(m_stopTrigger);
    addAction(m_startTrigger);

    m_camTrigger->stop();
}
void CamTriggerDialog::onExitAction(){
    if(m_camTrigger->isRunning())
    {
        QMessageBox confirmationBox;
        confirmationBox.setText(tr("CamTrigger is running."));
        confirmationBox.setInformativeText(tr("Do you want to stop trigger and exit"));
        confirmationBox.setStandardButtons(QMessageBox::Yes | QMessageBox::Cancel);
        if(confirmationBox.exec() != QMessageBox::Yes)
        {
            showExpanded();
            return;
        }
    }
//    m_camTrigger->saveSettings();//saved on saveUI
    exit(0);
}

void CamTriggerDialog::setOrientation(ScreenOrientation orientation)
{
#ifdef Q_OS_SYMBIAN
    if (orientation != ScreenOrientationAuto) {
#if defined(ORIENTATIONLOCK)
        const CAknAppUiBase::TAppUiOrientation uiOrientation =
                (orientation == ScreenOrientationLockPortrait) ? CAknAppUi::EAppUiOrientationPortrait
                    : CAknAppUi::EAppUiOrientationLandscape;
        CAknAppUi* appUi = dynamic_cast<CAknAppUi*> (CEikonEnv::Static()->AppUi());
        TRAPD(error,
            if (appUi)
                appUi->SetOrientationL(uiOrientation);
        );
        Q_UNUSED(error)
#else // ORIENTATIONLOCK
        qWarning("'ORIENTATIONLOCK' needs to be defined on Symbian when locking the orientation.");
#endif // ORIENTATIONLOCK
    }
#elif defined(Q_WS_MAEMO_5)
    Qt::WidgetAttribute attribute;
    switch (orientation) {
    case ScreenOrientationLockPortrait:
        attribute = Qt::WA_Maemo5PortraitOrientation;
        break;
    case ScreenOrientationLockLandscape:
        attribute = Qt::WA_Maemo5LandscapeOrientation;
        break;
    case ScreenOrientationAuto:
    default:
        attribute = Qt::WA_Maemo5AutoOrientation;
        break;
    }
    setAttribute(attribute, true);
#else // Q_OS_SYMBIAN
    Q_UNUSED(orientation);
#endif // Q_OS_SYMBIAN
}

void CamTriggerDialog::showExpanded()
{
#ifdef Q_OS_SYMBIAN
    // Toggle softkey visibility
    setWindowFlags( windowFlags() | Qt::WindowSoftkeysVisibleHint );
    showFullScreen();
#elif defined(Q_WS_MAEMO_5) || defined(Q_WS_MAEMO_6)
    showMaximized();
#else
    show();
#endif
}




void CamTriggerDialog::on_addTimeButton_clicked()
{
    addCaptureTimer(ui->addTimeEdit->time());
}

void CamTriggerDialog::addCaptureTimer(QTime time)
{
    QObjectList captureTimers = m_timersScrollWidget->children();
    foreach (QObject *child, captureTimers) {
        TimeEntity *timer = dynamic_cast<TimeEntity*>(child);
        if(timer){
            if(timer->time() == time)
            {
                QMessageBox::warning(this,tr("Timer Exist"), tr("there is another timer with the same time"));
                showExpanded();
                return;
            }
        }
    }
    TimeEntity *newTimer = new TimeEntity(time, m_timersScrollWidget);
     int col = (m_timersLayout->count() &1);
     int row = m_timersLayout->rowCount() - col;

     m_timersLayout->addWidget(newTimer,row,col,Qt::AlignLeft|Qt::AlignTop);
     ui->timersScrollArea->verticalScrollBar()->setValue(ui->timersScrollArea->verticalScrollBar()->maximum());

}


void CamTriggerDialog::on_saveToButton_clicked()
{
    ui->saveToEdit->setText( QFileDialog::getExistingDirectory(this, tr("Open Directory"),
                                                "",
                                               QFileDialog::ShowDirsOnly
                                               | QFileDialog::DontResolveSymlinks)
                            );

}

void CamTriggerDialog::on_addSendTimeButton_clicked()
{
    addSendTimer(ui->addSendTimeEdit->time(), ui->sendImgCountSpinBox->value());

}
void CamTriggerDialog::addSendTimer(QTime time, int imgCount)
{
    QObjectList sendTimers = m_sendTimersScrollWidget->children();
    foreach (QObject *child, sendTimers) {
        SendEntity *timer = dynamic_cast<SendEntity*>(child);
        if(timer){
            if(timer->time() == time)
            {
                QMessageBox::warning(this,tr("Timer Exist"), tr("there is another timer with the same time"));
                showExpanded();
                return;
            }
        }
    }

    SendEntity *newSend = new SendEntity(time, imgCount, m_sendTimersScrollWidget);
    m_sendTimersLayout->addWidget(newSend, m_sendTimersLayout->rowCount(), 0,Qt::AlignTop);
    ui->sendScrollArea->verticalScrollBar()->setValue(ui->sendScrollArea->verticalScrollBar()->maximum());

}

void CamTriggerDialog::onTurnOnCameraPreview()
{

#ifdef Q_OS_SYMBIAN
    turnOnBacklight();
    turnOnBacklightTimer->start();

#endif
    // show camera preview
    m_viewFinder->show();
}
void CamTriggerDialog::turnOnBacklight()
{
#ifdef Q_OS_SYMBIAN
//    TBool backlightState;
    //retrieve EBacklightState value
//    HAL::Get( HALData::EBacklightState, backlightState );
//    if(!backlightState)
        User::ResetInactivityTime();
#endif
}


void CamTriggerDialog::onTurnOffCameraPreview()
{
    // hide camera preview
    m_viewFinder->hide();
    turnOnBacklightTimer->stop();
}

void CamTriggerDialog::on_cropStartXSpinBox_editingFinished()
{
//    if(ui->cropEndXSpinBox->value() < ui->cropStartXSpinBox->value())
//        ui->cropEndXSpinBox->setValue(ui->cropStartXSpinBox->value());
    ui->cropEndXSpinBox->setMinimum(ui->cropStartXSpinBox->value()+1);
}

void CamTriggerDialog::on_cropStartYSpinBox_editingFinished()
{
//    if(ui->cropEndYSpinBox->value() < ui->cropStartYSpinBox->value())
//        ui->cropEndYSpinBox->setValue(ui->cropStartYSpinBox->value());
    ui->cropEndYSpinBox->setMinimum(ui->cropStartYSpinBox->value()+1);
}

void CamTriggerDialog::on_cropEndXSpinBox_editingFinished()
{
//    if(ui->cropStartXSpinBox->value() > ui->cropEndXSpinBox->value())
//        ui->cropStartXSpinBox->setValue(ui->cropEndXSpinBox->value());
    ui->cropStartXSpinBox->setMaximum(ui->cropEndXSpinBox->value()-1);
}

void CamTriggerDialog::on_cropEndYSpinBox_editingFinished()
{
//    if(ui->cropStartYSpinBox->value() > ui->cropEndYSpinBox->value())
//        ui->cropStartYSpinBox->setValue(ui->cropEndYSpinBox->value());
    ui->cropStartYSpinBox->setMaximum(ui->cropEndYSpinBox->value()-1);
}

void CamTriggerDialog::on_cameraResolutionComboBox_currentIndexChanged(int index)
{
    QSize currentResolution(ui->cameraResolutionComboBox->itemData(index).toSize());
    if(ui->cropStartXSpinBox->value() >= currentResolution.width())
        ui->cropStartXSpinBox->setValue(currentResolution.width()-1);
    if(ui->cropStartYSpinBox->value() >= currentResolution.height()-1)
        ui->cropStartYSpinBox->setValue(currentResolution.height());
    if(ui->cropEndXSpinBox->value() > currentResolution.width())
        ui->cropEndXSpinBox->setValue(currentResolution.width());
    if(ui->cropEndYSpinBox->value() > currentResolution.height())
        ui->cropEndYSpinBox->setValue(currentResolution.height());

    ui->cropStartXSpinBox->setMaximum(currentResolution.width()-1);
    ui->cropEndXSpinBox->setMaximum(currentResolution.width());
    ui->cropStartYSpinBox->setMaximum(currentResolution.height()-1);
    ui->cropEndYSpinBox->setMaximum(currentResolution.height());

    ui->resizeToWidthSpinBox->setValue(ui->cameraResolutionComboBox->itemData(index).toSize().width());
    ui->resizeToHeightSpinBox->setValue(ui->cameraResolutionComboBox->itemData(index).toSize().height());
}
