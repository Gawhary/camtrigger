#include "imgcapture.h"

ImgCapture::ImgCapture()
{
    Timers = new QSet<QTime>();
}

ImgCapture::~ImgCapture(){
    delete Timers;
}

void ImgCapture::addTimer(QTime time)
{
}

void ImgCapture::removeTimer(QTime time)
{
}

void ImgCapture::clearAllTimers()
{
}

void ImgCapture::start()
{
}

void ImgCapture::stop()
{
}
