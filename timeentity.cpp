#include "timeentity.h"
#include "ui_timeentity.h"
#include <QTime>

TimeEntity::TimeEntity(QTime time, QWidget *parent) :
    QWidget(parent),
    ui(new Ui::TimeEntity)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);
    m_time = time;
    ui->lcdNumber->display(m_time.toString("hh:mm"));

}

TimeEntity::~TimeEntity()
{
    delete ui;
}

QTime TimeEntity::time()
{
    return m_time;
}
