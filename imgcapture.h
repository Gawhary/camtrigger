#ifndef IMGCAPTURE_H
#define IMGCAPTURE_H

#include <QTime>
#include <QSet>

class ImgCapture
{


public:

    enum Status{ Stop, Run};

    ImgCapture();
    ~ImgCapture();
    void addTimer(QTime time);
    void removeTimer(QTime time);
    void clearAllTimers();
    void start();
    void stop();
    bool isRunning(){return status == Run;}
    bool containsTimer(QTime);


private:
    Status status;
    QSet<QTime> *Timers;


};

#endif // IMGCAPTURE_H
