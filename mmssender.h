#ifndef MMSSENDER_H
#define MMSSENDER_H


#include <QTime>
#include <QPair>

class MmsSender
{
public:
    enum Status{ stopped, running};

    MmsSender();
    void addTimer(QTime time, int lastImgs);
    void removeTimer(QTime time);
    void clearAllTimers();
    void start();
    void stop();
    bool isRunning(){return status == running;}


private:
    Status status;
    QList<QPair<QTime,int> > *QTimers;
};

#endif // MMSSENDER_H
