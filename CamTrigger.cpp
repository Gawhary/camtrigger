#include "CamTrigger.h"
#include <QDateTime>
#include <QMessage>
#include <QtDebug>
#include <QEventLoop>
#include <QSettings>
#include <QMetaObject>
#include <QMetaProperty>
//#include <QCameraImageProcessingControl>
#include <QSystemDeviceInfo>
#include <QImage>
//#include <QRgb>
//#include "imageProcessing.h"


CamTrigger::CamTrigger(QObject *parent) :
    QObject(parent)
{
    m_turnOnPreviewTimer = new QTimer(this);
    m_captureTimer = new QTimer(this);
    m_turnOffPreviewTimer = new QTimer(this);
    m_sendMmsTimer = new QTimer(this);

    //initializa camera
    m_camera = new QCamera(this);
    m_imageCapture = new QCameraImageCapture(m_camera, this);
    m_camera->setCaptureMode(QCamera::CaptureStillImage);
    m_camera->load();
    m_service = new QMessageService(this);


    m_running = false;
    m_viewFinder = NULL;

    m_preview = false;
    m_previewSecBeforeCapture = 30;
    m_previewDuration = 45;
//    m_turnOffDisplay = false;
    m_turnOffNetwork = false;
    m_flash = false;
    m_contrast = 0;
    m_brightnes = 0;
    m_zoom = 0;
    m_resolution = m_imageCapture->supportedResolutions().first();
    m_modefy = false;
    m_modefyQuality = -1;
    m_modefyResolution = m_resolution;
    m_modefyContrast = m_contrast;
    m_modefyBrightnes = m_brightnes;
    m_crop = false;
    m_modefyCropStart = QPoint(0,0);
    m_modefyCropEnd = QPoint(m_resolution.width(),m_resolution.height());
    m_fileNamePrefix = "Img";
    m_directory = QDir::homePath();
    m_sendMms = false;
//    m_sendMmsTo = ""


    m_turnOnPreviewTimer->setSingleShot(true);
    m_captureTimer->setSingleShot(true);
    m_turnOffPreviewTimer->setSingleShot(true);
    m_sendMmsTimer->setSingleShot(true);

    //connect timers
    connect(m_turnOnPreviewTimer, SIGNAL(timeout()), this, SLOT(onTurnOnPreviewTimeOut()));
    connect(m_turnOffPreviewTimer, SIGNAL(timeout()), this, SLOT(onTurnOffPreviewTimeOut()));
    connect(m_captureTimer, SIGNAL(timeout()), this, SLOT(onCaptureTimeOut()));
    connect(m_sendMmsTimer, SIGNAL(timeout()), this, SLOT(onSendMmsTimeOut()));

    connect(m_service, SIGNAL(stateChanged(QMessageService::State)), this, SLOT(mmsStateChanged(QMessageService::State)));

//#ifdef QT_DEBUG
//    connect(m_camera, SIGNAL(stateChanged(QCamera::State)), this, SLOT(cameraStateChanged(QCamera::State)));
//    connect(m_camera, SIGNAL(error(QCamera::Error)), this, SLOT(onCameraError(QCamera::Error)));
//    connect(m_imageCapture, SIGNAL(error(int,QCameraImageCapture::Error,QString)), this, SLOT(onImageCaptureError(int,QCameraImageCapture::Error,QString)));
//    connect(m_imageCapture, SIGNAL(imageCaptured(int,QImage)), this, SLOT(onImageCaptured(int,QImage)));
//    connect(m_imageCapture, SIGNAL(imageSaved(int,QString)), this, SLOT(onImageSaved(int,QString)));
//#endif



}

void CamTrigger::saveSettings()
{
    qDebug() << "CamTrigger::saveSettings() called";
    const QMetaObject* metaObject = this->metaObject();
    QSettings settings("CamTrigger");
    settings.clear();//clear saved settings
    for(int i = metaObject->propertyOffset(); i < metaObject->propertyCount(); ++i){
        if(metaObject->property(i).isStored()){
        QString name = QString::fromLatin1(metaObject->property(i).name());
        QVariant value = metaObject->property(i).read(this);
        settings.setValue(name, value);
        qDebug() << "Prorerty saved" << name << ": " << value;
        }
    }
    //save timers
//    settings.remove("captureTimers");
    settings.beginWriteArray("captureTimers",m_captureTimers.count());
    int i = 0;
    for (QMap<QTime,int>::iterator itr = m_captureTimers.begin(); itr != m_captureTimers.end(); itr++) {
        settings.setArrayIndex(i++);
        settings.setValue("time", itr.key());
        qDebug() << "Capture timer at:" << itr.key() << " saved";
    }
    settings.endArray();
//    settings.remove("sendTimers");
    settings.beginWriteArray("sendTimers", m_sendTimers.count());
    i = 0;
    for (QMap<QTime,int>::iterator itr = m_sendTimers.begin(); itr != m_sendTimers.end(); itr++) {
        settings.setArrayIndex(i++);
        settings.setValue("time", itr.key());
        settings.setValue("imgCount",itr.value());
        qDebug() << "Send timer at:" << itr.key() << " saved";
    }
    settings.endArray();
    settings.sync();
}

void CamTrigger::loadSettings()
{
    qDebug() << "CamTrigger::loadSettings() called";
    const QMetaObject* metaObject = this->metaObject();
    QSettings settings("CamTrigger");
    for(int i = metaObject->propertyOffset(); i < metaObject->propertyCount(); ++i){
        if(metaObject->property(i).isStored()){
            QString name = QString::fromLatin1(metaObject->property(i).name());
            if(settings.contains(name)){
                QVariant value = settings.value(name/*, metaObject->property(i).read(this)*/);
                metaObject->property(i).write(this,value);
                qDebug() << "Property loaded" << name << ": " << value;

            }
        }
    }
    //load timers
    int size = settings.beginReadArray("captureTimers");
    for(int i = 0; i < size; i++) {
        settings.setArrayIndex(i);
        addCaptureTimer(settings.value("time").toTime());
    }
    settings.endArray();

    size = settings.beginReadArray("sendTimers");
    for(int i = 0; i < size; i++) {
        settings.setArrayIndex(i);
        addSendTimer(settings.value("time").toTime(), settings.value("imgCount").toInt());
    }
    settings.endArray();
}

CamTrigger::~CamTrigger()
{
}

void CamTrigger::start(){
    qDebug() << "start() called";

    // if turn off network enabled turn off network when sending finished or canceled.
    // this feature is not supported in self singed version
#ifndef SELFSIGNED
    if(m_turnOffNetwork){
        XQProfile profile;
        //save current profile
        m_oriiginalProfile = profile.activeProfile();
        // change profile to offline
        bool error = profile.setActiveProfile(XQProfile::ProfileOffLine);
        if(!error){
            qDebug() << "profile switched to Offline.";
        }
    }
#endif

    if(m_camera->state() != QCamera::UnloadedState)
        m_camera->unload();
    // get first capture timer
    QTime current = QTime::currentTime();
    QTime nextCapture = getNextCaptureTimer(current);
    if(nextCapture.isValid()){
//        if(!m_viewFinder)
//            m_viewFinder = new QCameraViewfinder();
        m_camera->setViewfinder(m_viewFinder);
        // if preview enapled
        int msecToNextCapture = timeDiffMsec(current, nextCapture);
        if(m_preview){

            // calculate turn on display time
            QTime nextPreviewStart = nextCapture.addSecs( - m_previewSecBeforeCapture);
            int msecToNextPreview =  timeDiffMsec(current, nextPreviewStart);
            // start turn on display timer
//            m_turnOnPreviewTimer->setInterval(current.msecsTo(nextPreviewStart));
            if(msecToNextPreview >= msecToNextCapture)//preview should be started (odd case but happend while testing)
                m_captureTimer->setInterval(0);//start now
            else
                m_turnOnPreviewTimer->setInterval(msecToNextPreview);
            m_turnOnPreviewTimer->start();
            qDebug() <<"start(): m_turnOnPreviewTimer start called at" << current << "with target time: " << nextPreviewStart;
        }
        else {
            // start capture timer
//            m_captureTimer->setInterval(current.msecsTo(nextCapture));
            m_captureTimer->setInterval(msecToNextCapture);
            m_captureTimer->start();
            qDebug() << "start(): m_captreTimer start called at" << current << "with target time: " << nextCapture;
        }
    }
    else{
        qDebug() << "start(): there is no capture timers";
    }
    // if sendMms
    if(m_sendMms)   {
        // get first sendMms timer
        QTime nextSend = getNextSendTimer(current);
        if(nextSend.isValid()){
        // start sendMmsTimer(m_last...)
        m_nextSendImgCount = m_sendTimers[nextSend];
//        m_sendMmsTimer->setInterval(current.msecsTo(nextSend));
        m_sendMmsTimer->setInterval(timeDiffMsec(current, nextSend));
        m_sendMmsTimer->start();
        qDebug() << "start(): m_sendMmsTimer start called at" << current << "with target time: " << nextSend;
        }
        else{
            qDebug() << "start(): There is no send timers";
        }
    }
     m_running = true;
}
void CamTrigger::stop(){
    qDebug() << "stop() called";

    //stop all timers
    m_sendMmsTimer->stop();
    m_captureTimer->stop();
    m_turnOnPreviewTimer->stop();
    m_turnOffPreviewTimer->stop();

    if(m_camera->state() == QCamera::ActiveState)
        m_camera->stop();
    else if(m_camera->state() == QCamera::UnloadedState)
        m_camera->load();//load for supported capapilities inqueries
    m_imageCapture->cancelCapture();
    m_service->cancel();

//    if(!m_viewFinder->parent()){
//        m_viewFinder->deleteLater();
//        m_viewFinder = NULL;
//    }

    // if turn off network enabled turn on network before sending.
    // this feature is not supported in self singed version
#ifndef SELFSIGNED
    if(m_turnOffNetwork){
        XQProfile profile;
        //return saved profile
        bool error = profile.setActiveProfile(m_oriiginalProfile);
        if(!error){
            qDebug() << "profile switched back to Original.";
        }
    }
#endif

    m_running = false;
}


void CamTrigger::onCaptureTimeOut()
{
    qDebug() << "onCaptureTimeOut() called";
    // capture Img
    captureImg();
    //      get current time
    QTime current = QTime::currentTime();
    //      get next timer
    QTime nextCapture = getNextCaptureTimer(current);
    if(nextCapture.isValid()){
        int msecToNextCapture = timeDiffMsec(current, nextCapture);
        // if preview enabled,
        if(m_preview){
        //      calculate next turn on preview timer
            QTime nextPreviewStart = nextCapture.addSecs( - m_previewSecBeforeCapture);
            int msecToNextPreview =  timeDiffMsec(current, nextPreviewStart);
        //      start turn on preview timer

            if(msecToNextPreview >= msecToNextCapture)//preview should be started (odd case but happend while testing)
                m_captureTimer->setInterval(0);//start now
            else
                m_turnOnPreviewTimer->setInterval(msecToNextPreview);
            m_turnOnPreviewTimer->start();
            qDebug() << "onCaptureTimeOut(): m_turnOnPreviewTimer start called at" << current << "with target time: " << nextPreviewStart;
        }
        else{
            //      start capture timer
//            m_captureTimer->setInterval(current.msecsTo(nextCapture));
            m_captureTimer->setInterval(msecToNextCapture);
            m_captureTimer->start();
            qDebug() << "onCaptureTimeOut(): m_captureTimer start called at" << current << "with target time: " << nextCapture;
        }
    }
    else{
        qDebug() << "onCaptureTimeOut(): Error: can't find next capture timer";
    }
}

void CamTrigger::onTurnOnPreviewTimeOut()
{
    qDebug() << "onTurnOnPreviewTimeOut() called";
    // start capture timer ()
    m_captureTimer->setInterval(m_previewSecBeforeCapture * 1000);
    m_captureTimer->start();
    qDebug() << "onTurnOnPreviewTimeOut(): m_captureTimer start called at" << QTime::currentTime() << "with target time: " << QTime::currentTime().addSecs(m_previewSecBeforeCapture);
    // start turn off preview timer
    m_turnOffPreviewTimer->setInterval(m_previewDuration * 1000);
    m_turnOffPreviewTimer->start();
    qDebug() << "onTurnOnPreviewTimeOut(): m_turnOffPreviewTimer start called at" << QTime::currentTime() << "with target time: " << QTime::currentTime().addSecs(m_previewDuration);

    startCamera();
    emit turnOnCameraPreview();

}

void CamTrigger::onTurnOffPreviewTimeOut()
{
    qDebug() << "onTurnOffPreviewTimeOut() called";
    if(m_previewDuration > m_previewSecBeforeCapture){
        if(m_camera->state() != QCamera::UnloadedState){
            m_camera->unload();
            qDebug() << "onTurnOffPreviewTimeOut(): Camera unload called";
        }
    }
    emit turnOffCameraPreview();

}

void CamTrigger::onSendMmsTimeOut()
{
    qDebug() << "onSendMmsTimeOut() called";
    // get current time
    QTime current = QTime::currentTime();
    // sendMms(m_lastImgsForNextSend)
    sendMms(m_nextSendImgCount);
    // get next send time and lastImgs
    QTime nextSend = getNextSendTimer(current);
    if(nextSend.isValid()){
        // set last Imgs For Next Send
        m_nextSendImgCount = m_sendTimers[nextSend];
        // start sendMmsTimer
//        m_sendMmsTimer->setInterval(current.msecsTo(nextSend));
        m_sendMmsTimer->setInterval(timeDiffMsec(current, nextSend));
        m_sendMmsTimer->start();
        qDebug() << "onSendMmsTimeOut(): m_sendMmsTimer start called at" << current << "with target time: " << nextSend;
    }
    else{
        qDebug() << "onSendMmsTimeOut(): Error: can't find next send timer";
    }
}


void CamTrigger::startCamera()
{
    qDebug() << "startCameraPreview() called";
//    if (m_camera->state() == QCamera::ActiveState)
//        m_camera->stop();
    m_camera->start();
    qDebug() << "onTurnOnPreviewTimeOut(): Camera start called";
    waitForCameraStart(10000);
    applyCameraSettings();

}

void CamTrigger::captureImg()
{
    //DEBUG
    qDebug() << "captureImg called at " << QTime::currentTime().toString();

    if(!m_preview){
        startCamera();
    }
    else
        if(m_camera->state() != QCamera::ActiveState){//should never happen
            m_camera->start();
            qDebug() << "captureImg() Camera start called";
        }
    if(waitForCaptureReady(30000))//with timeout of 30 sec
    {
//        applyCameraSettings();// called in start preview
        m_camera->searchAndLock();

        QString fileName = QString("%1%2%3_%4.jpg").arg(m_directory).arg(/*'/'*/QDir::separator()).arg(m_fileNamePrefix).arg(QDateTime::currentDateTime().toString("yyyyMMddhhmmss"));
        m_imageCapture->capture(fileName);
        qDebug() << "captureImg: captur called with fileName: " << fileName;

        //wait for capture completed or error
        waitForCapture(30000);//with timeout of 30 sec
        m_camera->unlock();

    }
    if (!m_preview || m_previewDuration <= m_previewSecBeforeCapture){
        if(m_camera->state() != QCamera::UnloadedState){
            m_camera->unload();
            qDebug() << "captureImg(): Camera unload called";
        }
    }
//    else{
//        //fix camera preview
//        m_camera->stop();
//        qDebug() << "captureImg: camera stop called";
//    }
}

void CamTrigger::applyCameraSettings()
{
    qDebug() << "CamTrigger::applyCameraSettings() called";
    QCameraExposure *exposure = m_camera->exposure();
    QCameraFocus *focus = m_camera->focus();
    QCameraImageProcessing *imageProcessing = m_camera->imageProcessing();

    if(m_flash)
        exposure->setFlashMode(QCameraExposure::FlashOn);
    else
        exposure->setFlashMode(QCameraExposure::FlashOff);
    imageProcessing->setContrast(m_contrast);
    exposure->setExposureCompensation(qreal(m_brightnes)*0.02);
    focus->zoomTo(0,focus->maximumDigitalZoom()*m_zoom);

    //TODO: optimize (low periority)
    QImageEncoderSettings settingEncoding = m_imageCapture->encodingSettings();
    settingEncoding.setResolution(m_resolution);
    m_imageCapture->setEncodingSettings(settingEncoding);

//    m_imageCapture->encodingSettings().setResolution(m_resolution.width(),m_resolution.height());
}

bool CamTrigger::waitForCaptureReady(int timeoutMsec)
{
    while (!m_imageCapture->isReadyForCapture() || m_camera->status() != QCamera::ActiveStatus){
        qDebug() << "captureImg: Camera not ready, waiting...";
        QEventLoop loop;
        connect(m_imageCapture, SIGNAL(readyForCaptureChanged(bool)), &loop, SLOT(quit()));
        connect(m_camera,SIGNAL(statusChanged(QCamera::Status)), &loop, SLOT(quit()));
        QTimer timer;
        timer.setInterval(timeoutMsec);
        timer.setSingleShot(true);
        timer.start();
        connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

        loop.exec();
        if(!timer.isActive())
            break;
    }
    if(!m_imageCapture->isReadyForCapture()){
        qDebug() << "captureImg: waiting for capture ready timeout.";
        return false;
    }
    else{
        qDebug() << "captureImg: Camera is now ready.";
        return true;
    }
}

bool CamTrigger::waitForCameraStart(int timeoutMsec)
{
    while ( m_camera->status() != QCamera::ActiveStatus){
        qDebug() << "captureImg: Camera not active, waiting...";
        QEventLoop loop;
        connect(m_camera,SIGNAL(statusChanged(QCamera::Status)), &loop, SLOT(quit()));
        QTimer timer;
        timer.setInterval(timeoutMsec);
        timer.setSingleShot(true);
        timer.start();
        connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

        loop.exec();
        if(!timer.isActive())
            break;
    }
    if(m_camera->status() != QCamera::ActiveStatus){
        qDebug() << "captureImg: waiting for camera start timeout.";
        return false;
    }
    else{
        qDebug() << "captureImg: Camera is now active.";
        return true;
    }
}

bool CamTrigger::waitForCapture(int timeoutMsec)
{
    QEventLoop loop;
    QObject::connect(m_imageCapture, SIGNAL(imageExposed(int)), &loop, SLOT(quit()));
    QObject::connect(m_imageCapture, SIGNAL(imageCaptured(int,QImage)), &loop, SLOT(quit()));
    QObject::connect(m_imageCapture, SIGNAL(error(int,QCameraImageCapture::Error,QString)), &loop, SLOT(quit()));
    QObject::connect(m_imageCapture, SIGNAL(imageSaved(int,QString)), &loop, SLOT(quit()));
    QObject::connect(m_camera, SIGNAL(error(QCamera::Error)), &loop, SLOT(quit()));

    QTimer timer;
    timer.setInterval(timeoutMsec);
    timer.setSingleShot(true);
    timer.start();
    connect(&timer, SIGNAL(timeout()), &loop, SLOT(quit()));

    loop.exec();
    if(!timer.isActive()){
        qDebug() << "captureImg(): Waiting for capture timeout.";
        return false;
    }
    else
        return true;
}

void CamTrigger::sendMms(int lastImgsCount)
{
    //DEBUG
    qDebug() << "sendMms::sendMms called at" << QTime::currentTime().toString();

    // if turn off network enabled turn on network before sending.
    // this feature is not supported in self singed version
#ifndef SELFSIGNED
    if(m_turnOffNetwork){
        XQProfile profile;
        bool error = profile.setActiveProfile(XQProfile::ProfileSilent);
        if(!error){
            qDebug() << "profile switched to Silent.";
        }
    }
#endif

    QMessage message;
    message.setType(QMessage::Mms);

    message.setTo(QMessageAddress(QMessageAddress::Phone, m_mmsDestination));

    QStringList files;
    QStringList pathes;
    QDir dir(m_directory);
    files = dir.entryList(QDir::Files);

    // extract date part from names and sort by date part
    QMap<QString,QString> map;
    map.insert("","");
    qDebug() << "sendMms: files in directory list: ";
    foreach(QString file, files){
        map.insert(file.right(18),m_directory + QDir::separator() + file);
        qDebug() << file;
    }


//    for(QMap<QString,QString>::iterator i = map.begin() ,count = 0 ; i != map.end(), count < lastImgsCount; i++,count++ )
//        pathes.append(i.value());
    QMap<QString,QString>::iterator itr = map.end();
    for(int i = 0 ;i < lastImgsCount; i++ )
    {
        itr--;
        if(itr == map.begin())
            break;
        pathes.append(itr.value());

    }

    if(m_modefy){
        //TODO: modefy imeges in pathes (outplace)
        pathes = modefyImages(pathes);
    }

    // add battery status as message body
    QSystemDeviceInfo deviceInfo;
    QString text = QString("Battery level: %1%").arg(deviceInfo.batteryLevel());

    message.appendAttachments(pathes);
    message.setBody(text);

    //DEBUG
    qDebug() << "sendMms: Message Ready\nTo:" <<  m_mmsDestination << " Text:" << text << "\nFiles:\n";
    foreach(const QString& path, pathes)
        qDebug() << path;
    //DEBUG end

    bool initiated =  m_service->send(message);
    if(initiated){
        qDebug() << "sendMms: send message operation initiated ";
        if(m_modefy){
            //delete modefied images
            foreach (QString filePath, pathes) {
                bool removed = QFile(filePath).remove();
                if(removed){
                    qDebug() << "Modefied file: " << filePath << "has been deleted";
                }
            }
        }
    }
}

inline int calculateContrast(int value,int contrast)
{
    return ((( value - 127 ) * contrast)>>7) + 127;
}

QList<QString> CamTrigger::modefyImages(QList<QString> inputImageFiles)
{
    QList<QString> modefiedImageFiles;
    QString path(m_directory + QDir::separator() + "modefied");
    QDir dir(path);
    if(!dir.exists())
    dir.mkpath(path);
    foreach (QString file, inputImageFiles) {
        QImage image(file);
        if(!image.isNull()){
            //process image

            //crop
            if(m_crop)
                image = image.copy(m_modefyCropStart.x(),
                                         m_modefyCropStart.y(),
                                         m_modefyCropEnd.x() - m_modefyCropStart.x(),
                                         m_modefyCropStart.y() - m_modefyCropStart.y()
                                         );

            //scale
            image = image.scaled(m_modefyResolution);

            //change contrast
            //change brightness
            if(m_modefyContrast || m_modefyBrightnes)
            {
                int contrast = (m_modefyContrast + 128);
                int brightness = m_modefyBrightnes << 1 ;// *2 change brightness range from (-100,100) to (-200 , 200)

                QImage im = image;
                im.detach();
                if( im.numColors() == 0 ) /* truecolor */
                    {
                    if( im.format() != QImage::Format_RGB32 ) /* just in case */
                        im = im.convertToFormat( QImage::Format_RGB32 );
                    int table[ 256 ];
                    for( int i = 0; i < 256; ++i )
                    {
                        int  value = i;
                        value = calculateContrast(value,contrast);//apply contrast
                        value += brightness;//apply brightness
                        if(value < 0) value = 0;
                        else if(value > 255) value = 255;
                        table[ i ] = value;

                    }
                    if( im.hasAlphaChannel() )
                        {
                        for( int y = 0;
                             y < im.height();
                             ++y )
                            {
                            QRgb* line = reinterpret_cast< QRgb* >( im.scanLine( y ));
                            for( int x = 0;
                                 x < im.width();
                                 ++x )
                                line[ x ] = qRgba(
                                                    table[ qRed( line[ x ] )],
                                                    table[ qGreen( line[ x ] )],
                                                    table[ qBlue( line[ x ] )],
                                                    table[ qAlpha( line[ x ] )]);
                            }
                        }
                    else
                        {
                        for( int y = 0;
                             y < im.height();
                             ++y )
                            {
                            QRgb* line = reinterpret_cast< QRgb* >( im.scanLine( y ));
                            for( int x = 0;
                                 x < im.width();
                                 ++x )
                                line[ x ] = qRgb(
                                                    table[ qRed( line[ x ] )],
                                                    table[ qGreen( line[ x ] )],
                                                    table[ qBlue( line[ x ] )]);
                            }
                        }
                    }
                else
                    {
                        QVector<QRgb> colors = im.colorTable();
                        for( int i = 0; i < im.numColors(); ++i ){
                            int r = qRed( colors[ i ] );
                            int g = qGreen( colors[ i ] );
                            int b = qBlue( colors[ i ] );

                            r = calculateContrast(r,contrast);
                            g = calculateContrast(g,contrast);
                            b = calculateContrast(b,contrast);

                            r += brightness;
                            g += brightness;
                            b += brightness;

                            if(r < 0) r = 0;else if(r > 255) r = 255;
                            if(g < 0) g = 0;else if(g > 255) g = 255;
                            if(b < 0) b = 0;else if(b > 255) b = 255;

                            colors[ i ] = qRgb(r,g,b);
                        }
                    }
                image = im;
            }


            // save image with target quality
            QString modefiedFile(path + QDir::separator() + QFileInfo(file).fileName());
            image.save(modefiedFile,0,m_modefyQuality );
            qDebug() << "modefyImages: Modefied image saved to: " << modefiedFile;
            modefiedImageFiles.append(modefiedFile);
        }
    }
    return modefiedImageFiles;
}


void CamTrigger::addCaptureTimer(QTime time)
{
    m_captureTimers.insert(time, 0);
}

void CamTrigger::addSendTimer(QTime time, int lastImgs)
{
    m_sendTimers.insert(time, lastImgs);
}

void CamTrigger::removeCaptureTimer(QTime time)
{
    m_captureTimers.remove(time);
}

void CamTrigger::removeSendTimer(QTime time)
{
    m_sendTimers.remove(time);
}

void CamTrigger::clearCaptureTimers()
{
    m_captureTimers.clear();
}

void CamTrigger::clearSendTimers()
{
    m_sendTimers.clear();
}

void CamTrigger::setCameraView(QCameraViewfinder* cameraView)
{
    m_viewFinder = cameraView;
}

QTime CamTrigger::getNextCaptureTimer(QTime current)
{
    if(m_captureTimers.isEmpty())
        return QTime();
    foreach (QTime t, m_captureTimers.keys()) {
        if(t > current)
            return t;
    }
    return m_captureTimers.begin().key();
}

QTime CamTrigger::getNextSendTimer(QTime current)
{
    if(m_sendTimers.isEmpty())
        return QTime();
    foreach (QTime t, m_sendTimers.keys()) {
        if(t > current)
            return t;
    }
    return m_sendTimers.begin().key();
}


bool CamTrigger::isPreviewEnabled() const
{
    return m_preview;
}

int CamTrigger::previewSecBeforeCapture() const
{
    return m_previewSecBeforeCapture;
}

int CamTrigger::previewDuration() const
{
    return m_previewDuration;
}

QString CamTrigger::fileNamePrefix() const
{
    return m_fileNamePrefix;
}

QString CamTrigger::directory() const
{
    return m_directory;
}

bool CamTrigger::isSendMmsEnabled() const
{
    return m_sendMms;
}

QString CamTrigger::mmsDistenation() const
{
    return m_mmsDestination;
}


void CamTrigger::enablePreview(bool arg)
{
    m_preview = arg;
}

void CamTrigger::setPreviewSecBeforeCapture(int arg)
{
    m_previewSecBeforeCapture = arg;
}

void CamTrigger::setPreviewDuration(int arg)
{
    m_previewDuration = arg;
}

void CamTrigger::setFileNamePrefix(QString arg)
{
    m_fileNamePrefix = arg;
}

void CamTrigger::setDirectory(QString arg)
{
    m_directory = arg;
}

void CamTrigger::enableSendMms(bool arg)
{
    m_sendMms = arg;
}

void CamTrigger::setMmsDstenation(QString arg)
{
    m_mmsDestination = arg;
}
bool CamTrigger::isRunning() const
{
    return m_running;
}

void CamTrigger::mmsStateChanged(QMessageService::State state)
{

    // if turn off network enabled turn off network when sending finished or canceled.
    // this feature is not supported in self singed version
#ifndef SELFSIGNED
    if(m_turnOffNetwork){
        if ((   state == QMessageService::FinishedState) ||
               (state == QMessageService::CanceledState)){
            XQProfile profile;
            bool error = profile.setActiveProfile(XQProfile::ProfileOffLine);
            if(!error){
                qDebug() << "profile switched to Offline.";
            }

        }
    }
#endif

    if (state == QMessageService::CanceledState)
        qDebug() << "Sending MMS was canceled.";
    else if (state == QMessageService::FinishedState)
        qDebug() << "Sending MMS  has finished execution; succesfully completed or otherwise.";
    else if (state == QMessageService::ActiveState)
        qDebug() << "sending MMS is currently executing";
    else if (state == QMessageService::InactiveState)
        qDebug() << "sending MMS has not yet begun execution.";

}

void CamTrigger::cameraStateChanged(QCamera::State state) {
    if (state == QCamera::UnloadedState)
        qDebug() << "Camera state changed to Unloaded.";
    else if (state == QCamera::LoadedState)
        qDebug() << "Camera state changed to Loaded(loaded or stoped).";
    else if (state == QCamera::ActiveState)
        qDebug() << "Camera state changed to Active(started).";

}

int CamTrigger::timeDiffMsec(QTime from, QTime to) {
    int msDiff = from.msecsTo(to);
    if(msDiff >= 0)
        return msDiff;
    else//go to next day
        return msDiff + 86400000;// mSec in day(24 * 60 * 60 * 1000)
}

void CamTrigger::onCameraError(QCamera::Error error)
{
    qDebug() << m_camera->errorString();
}

void CamTrigger::onImageCaptured ( int id, const QImage& ){
    qDebug() << "frame with request:" << id << "captured, but not processed and saved yet.";
}
void CamTrigger::onImageSaved ( int id, const QString& fileName ){
    qDebug() << "frame with request: " << id << " saved to: " << fileName;

}
void CamTrigger::onImageCaptureError ( int id, QCameraImageCapture::Error error, const QString & errorString ){
    qDebug() << "capture request :" << id << " has failed with error " << error << " and description: " << errorString;
}

bool CamTrigger::isModefyEnabled() const
{
    return m_modefy;
}

void CamTrigger::enableModefy(bool arg)
{
    m_modefy = arg;
}

void CamTrigger::enableTurnOffNetwork(bool arg)
{
    m_turnOffNetwork = arg;
}

//void CamTrigger::enableTurnOffDisplay(bool arg)
//{
//    m_turnOffDisplay = arg;
//}

void CamTrigger::enableFlash(bool arg)
{
    m_flash = arg;
}

//void CamTrigger::setFocus(qreal arg)
//{
//    m_focus = arg;
//}

//void CamTrigger::enableAutoFocus(bool arg)
//{
//    m_autoFocus = arg;
//}

void CamTrigger::setContrast(int arg)
{
    m_contrast = arg;
}

void CamTrigger::setBrightnes(int arg)
{
    m_brightnes = arg;
}

void CamTrigger::setZoom(qreal arg)
{
    m_zoom = arg;
}

void CamTrigger::setModefyQuality(int arg)
{
    m_modefyQuality = arg;
}

void CamTrigger::setModefyBrightnes(int arg)
{
    m_modefyBrightnes = arg;
}

void CamTrigger::setModefyContrast(int arg)
{
    m_modefyContrast = arg;
}

void CamTrigger::setModefyCropEnd(QPoint arg)
{
    m_modefyCropEnd = arg;
}

void CamTrigger::setModefyCropStart(QPoint arg)
{
    m_modefyCropStart = arg;
}

QList<QSize> CamTrigger::supportedResolutions()
{

    if(m_camera->state() == QCamera::UnloadedState)
        m_camera->load();
    return m_imageCapture->supportedResolutions(m_imageCapture->encodingSettings());
}

QList<QTime> CamTrigger::captureTimers()
{
    return m_captureTimers.keys();
}

QMap<QTime, int> CamTrigger::sendTimers()
{
    return m_sendTimers;
}

void CamTrigger::enableCrop(bool arg)
{
    m_crop = arg;
}

void CamTrigger::setModefyResolution(QSize arg)
{
    m_modefyResolution = arg;
}

void CamTrigger::setFocusMode(int arg)
{
    m_focusMode = arg;
}

void CamTrigger::setResolution(QSize arg)
{
    m_resolution = arg;
}

//bool CamTrigger::isTurnOffDisplayEnabled() const
//{
//    return m_turnOffDisplay;
//}

bool CamTrigger::isFlashEnabled() const
{
    return m_flash;
}

//qreal CamTrigger::focus() const
//{
//    return m_focus;
//}

//bool CamTrigger::isAutoFocusEnabled() const
//{
//    return m_autoFocus;
//}

int CamTrigger::contrast() const
{
    return m_contrast;
}

int CamTrigger::brightnes() const
{
    return m_brightnes;
}

qreal CamTrigger::zoom() const
{
    return m_zoom;
}

int CamTrigger::modefyQuality() const
{
    return m_modefyQuality;
}

int CamTrigger::modefyBrightnes() const
{
    return m_modefyBrightnes;
}

int CamTrigger::modefyContrast() const
{
    return m_modefyContrast;
}

QPoint CamTrigger::modefyCropEnd() const
{
    return m_modefyCropEnd;
}

QPoint CamTrigger::modefyCropStart() const
{
    return m_modefyCropStart;
}

bool CamTrigger::isTurnOffNetworkEnabled() const
{
    return m_turnOffNetwork;
}

QSize CamTrigger::modefyResolution() const
{
    return m_modefyResolution;
}

bool CamTrigger::isCropEnabled() const
{
    return m_crop;
}

bool CamTrigger::isFlashSupported() const
{
    if(m_camera->state() == QCamera::UnloadedState)
        m_camera->load();
    return m_camera->exposure()->isFlashModeSupported(QCameraExposure::FlashOn);
}

QMap<QString, int> CamTrigger::supportedFocusModes() const
{
    if(m_camera->state() == QCamera::UnloadedState)
        m_camera->load();

    QMap<QString, int> supportedFocusModes;
    if(m_camera->focus()->isFocusModeSupported(QCameraFocus::ManualFocus))
        supportedFocusModes.insert("ManualFocus",QCameraFocus::ManualFocus);
    if(m_camera->focus()->isFocusModeSupported(QCameraFocus::HyperfocalFocus))
        supportedFocusModes.insert("HyperfocalFocus",QCameraFocus::HyperfocalFocus);
    if(m_camera->focus()->isFocusModeSupported(QCameraFocus::InfinityFocus))
        supportedFocusModes.insert("InfinityFocus",QCameraFocus::InfinityFocus);
    if(m_camera->focus()->isFocusModeSupported(QCameraFocus::AutoFocus))
        supportedFocusModes.insert("AutoFocus",QCameraFocus::AutoFocus);
    if(m_camera->focus()->isFocusModeSupported(QCameraFocus::ContinuousFocus))
        supportedFocusModes.insert("ContinuousFocus",QCameraFocus::ContinuousFocus);
    if(m_camera->focus()->isFocusModeSupported(QCameraFocus::MacroFocus))
        supportedFocusModes.insert("MacroFocus",QCameraFocus::MacroFocus);
    return supportedFocusModes;
}

int CamTrigger::focusMode() const
{
    return m_focusMode;
}

QSize CamTrigger::resolution() const
{
    return m_resolution;
}








