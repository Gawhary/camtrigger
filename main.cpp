#include "camtriggerdialog.h"

#include <QtGui/QApplication>



int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    CamTriggerDialog mainWindow;
    mainWindow.setOrientation(CamTriggerDialog::ScreenOrientationLockPortrait);
    mainWindow.showExpanded();

    return app.exec();
}
