#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include <QMap>
#include <QPair>
#include <QDir>
#include <QTime>
#include <QTimer>
#include <QCamera>
#include <QCameraViewfinder>
#include <QCameraImageCapture>
#include <qmessage.h>
#include <middleware/QMessageService.h>
#include <QPair>


#ifndef SELFSIGNED
#include "XQProfile.h"//Unsigned version  (turn off network)
#endif

QTM_USE_NAMESPACE

class CamTrigger :public QObject
{
    Q_OBJECT

//    Q_DECLARE_METATYPE(QPair<QSize,QSize>)

    //Display
    Q_PROPERTY(bool preview READ isPreviewEnabled WRITE enablePreview)
    Q_PROPERTY(int previewSecBeforeCapture READ previewSecBeforeCapture  WRITE setPreviewSecBeforeCapture )
    Q_PROPERTY(int previewDuration READ previewDuration WRITE setPreviewDuration)
//    Q_PROPERTY(bool turnOffDisplay READ isTurnOffDisplayEnabled WRITE enableTurnOffDisplay)
    Q_PROPERTY(bool turnOffNetwork READ isTurnOffNetworkEnabled WRITE enableTurnOffNetwork)
    //Camera
    Q_PROPERTY(bool flash READ isFlashEnabled WRITE enableFlash)
//    Q_PROPERTY(bool autoFocus READ isAutoFocusEnabled WRITE enableAutoFocus)
//    Q_PROPERTY(qreal focus READ focus WRITE setFocus)
    Q_PROPERTY(int focusMode READ focusMode WRITE setFocusMode)
    Q_PROPERTY(int contrast READ contrast WRITE setContrast)
    Q_PROPERTY(int brightnes READ brightnes WRITE setBrightnes)
    Q_PROPERTY(qreal zoom READ zoom WRITE setZoom)
    Q_PROPERTY(QSize resolution READ resolution WRITE setResolution)
    //Modefy
    Q_PROPERTY(bool modefy READ isModefyEnabled WRITE enableModefy)
    Q_PROPERTY(int modefyQuality READ modefyQuality WRITE setModefyQuality)
    Q_PROPERTY(QSize modefyResolution READ modefyResolution WRITE setModefyResolution)
    Q_PROPERTY(bool crop READ isCropEnabled WRITE enableCrop)
    Q_PROPERTY(int modefyContrast READ modefyContrast WRITE setModefyContrast)
    Q_PROPERTY(int modefyBrightnes READ modefyBrightnes WRITE setModefyBrightnes)
//    Q_PROPERTY(QPair<QSize,QSize> modefyCrop READ modefyCrop WRITE setModefyCrop)
    Q_PROPERTY(QPoint modefyCropStart READ modefyCropStart WRITE setModefyCropStart)
    Q_PROPERTY(QPoint modefyCropEnd READ modefyCropEnd WRITE setModefyCropEnd)
    //Storage
    Q_PROPERTY(QString fileNamePrefix READ fileNamePrefix WRITE setFileNamePrefix)
    Q_PROPERTY(QString directory READ directory WRITE setDirectory)
    //Send
    Q_PROPERTY(bool sendMms READ isSendMmsEnabled WRITE enableSendMms)
    Q_PROPERTY(QString sendMmsTo READ mmsDistenation WRITE setMmsDstenation)

    Q_PROPERTY(bool running READ isRunning STORED false NOTIFY statusChanged)



public:


    explicit CamTrigger(QObject *parent = 0);
    ~CamTrigger();

signals:
    void turnOnCameraPreview();
    void turnOffCameraPreview();

    void statusChanged(bool arg);

    public slots:
    void saveSettings();
    void loadSettings();

    void addCaptureTimer(QTime);
    void addSendTimer(QTime, int lastImgs = 10);
    void removeCaptureTimer(QTime);
    void removeSendTimer(QTime);
    void clearCaptureTimers();
    void clearSendTimers();
    void start();
    void stop();

    void setCameraView(QCameraViewfinder*);
    QList<QSize> supportedResolutions();

    void enablePreview(bool arg);
    void setPreviewSecBeforeCapture(int arg);
    void setPreviewDuration(int arg);
    void setFileNamePrefix(QString arg);
    void setDirectory(QString arg);
    void enableSendMms(bool arg);
    void setMmsDstenation(QString arg);
    void enableModefy(bool arg);
    void enableTurnOffNetwork(bool arg);
//    void enableTurnOffDisplay(bool arg);
    void enableFlash(bool arg);
//    void setFocus(qreal arg);
//    void enableAutoFocus(bool arg);
    void setContrast(int arg);
    void setBrightnes(int arg);
    void setZoom(qreal arg);
    void setModefyQuality(int arg);
    void setModefyBrightnes(int arg);
    void setModefyContrast(int arg);
    void setModefyCropEnd(QPoint arg);
    void setModefyCropStart(QPoint arg);
    void setModefyResolution(QSize arg);
    void enableCrop(bool arg);

    void setFocusMode(int arg);
    void setResolution(QSize arg);

    private slots:
    void onCaptureTimeOut();
    void onTurnOnPreviewTimeOut();
    void onTurnOffPreviewTimeOut();
    void onSendMmsTimeOut();
    void captureImg();
    void sendMms(int lastImgsCount);

    void mmsStateChanged(QMessageService::State);
    void cameraStateChanged(QCamera::State state);
    void onCameraError(QCamera::Error error);
    void onImageCaptured(int id, const QImage &preview);
    void onImageSaved(int id, const QString &fileName);
    void onImageCaptureError(int id, QCameraImageCapture::Error error, const QString &errorString);
    void applyCameraSettings();
    void startCamera();
//    void stopCamera();


private:

    QTime getNextCaptureTimer(QTime current);
    QTime getNextSendTimer(QTime current);
    int timeDiffMsec(QTime from, QTime to);
    bool waitForCaptureReady(int timeoutMsec);
    bool waitForCapture(int timeoutMsec);
    bool waitForCameraStart(int timeoutMsec);
    QList<QString> modefyImages(QList<QString>);
//    QImage changeImageBrightness(const QImage &image, int value);

public:

    bool isPreviewEnabled() const;
    int previewSecBeforeCapture() const;
    int previewDuration() const;
    QString fileNamePrefix() const;
    QString directory() const;
    bool isSendMmsEnabled() const;
    QString mmsDistenation() const;
    bool isRunning() const;
    bool isModefyEnabled() const;
//    bool isTurnOffDisplayEnabled() const;
    bool isFlashEnabled() const;
//    qreal focus() const;
//    bool isAutoFocusEnabled() const;
    int contrast() const;
    int brightnes() const;
    qreal zoom() const;
    int modefyQuality() const;
    int modefyBrightnes() const;
    int modefyContrast() const;
    QPoint modefyCropEnd() const;
    QPoint modefyCropStart() const;
    bool isTurnOffNetworkEnabled() const;
    QSize modefyResolution() const;
    QMap<QString,int> supportedFocusModes() const;

    QList<QTime> captureTimers();
    QMap<QTime, int> sendTimers();

    bool isCropEnabled() const;

    bool isFlashSupported() const;

    int focusMode() const;
    QSize resolution() const;


    private:
    QTimer *m_captureTimer;
    QTimer *m_turnOnPreviewTimer;
    QTimer *m_turnOffPreviewTimer;
    QTimer *m_sendMmsTimer;

    QMap<QTime, int> m_captureTimers;
    QMap<QTime, int> m_sendTimers;

    QCamera *m_camera;
    QCameraViewfinder *m_viewFinder;
    QCameraImageCapture *m_imageCapture;    
    QMessageService* m_service;

    bool m_preview;
    int m_previewSecBeforeCapture;
    int m_previewDuration;
    QString m_fileNamePrefix;
    QString m_directory;
    bool m_sendMms;
    QString m_mmsDestination;
    bool m_running;
    int m_nextSendImgCount;
    bool m_modefy;
    bool m_turnOffNetwork;
//    bool m_turnOffDisplay;
    bool m_flash;
//    qreal m_focus;
//    bool m_autoFocus;
    int m_contrast;
    int m_brightnes;
    qreal m_zoom;
    int m_modefyQuality;
    int m_modefyBrightnes;
    int m_modefyContrast;
    QPoint m_modefyCropEnd;
    QPoint m_modefyCropStart;
    QSize m_modefyResolution;
    bool m_crop;
    int m_focusMode;
    QSize m_resolution;

#ifndef SELFSIGNED
    XQProfile::Profile m_oriiginalProfile;
#endif
};

#endif // SETTINGS_H
